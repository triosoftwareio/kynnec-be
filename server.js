import express from 'express';
import socketIO from 'socket.io';
import config from './config';
import middleware from './app/middleware';

global.config = config;

const app = express();
const server = app.listen(8080);
const io = socketIO(server);

middleware(app, express, io);

export default { app };

//module.exports = app;
