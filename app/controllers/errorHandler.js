import {
  SOMETHING_GOES_WRONG,
  USER_EXIST,
  WRONG_CREDENTIALS,
  MISSED_PARAMS,
  MISSED_ACCESS_TOKEN,
  ACCESS_RESTRICTED,
  CUSTOM_ERROR,
} from '../shared/constants';

const errorConstructor = (errorConst, errorDetails = {}) => {
  const statuses = {
    200: 'OK',
    201: 'Created',
    400: 'Bad Request',
    304: 'Not Modified',
    401: 'Unauthorized',
    403: 'Forbidden',
    404: 'Not Found',
    409: 'Conflict',
    422: 'Unprocessable Entity',
    500: 'Internal server error',
  };

  const missedParamsMessage =
    errorDetails.missedParams || errorDetails.missedBody
      ? `Missed params - ${errorDetails.missedParams.join(
          ', ',
        )} ${errorDetails.missedBody ? 'request body' : ''}`
      : '';

  const errorObject = {
    [SOMETHING_GOES_WRONG]: {
      statusCode: 500,
      status: statuses[500],
      message: missedParamsMessage || statuses[500],
    },
    [USER_EXIST]: {
      statusCode: 409,
      status: statuses[409],
      message: missedParamsMessage || 'User already exist',
    },
    [WRONG_CREDENTIALS]: {
      statusCode: 401,
      status: statuses[403],
      message: missedParamsMessage || 'Wrong credentials',
    },
    [MISSED_PARAMS]: {
      statusCode: 422,
      status: statuses[422],
      message: missedParamsMessage || statuses[422],
    },
    [MISSED_ACCESS_TOKEN]: {
      statusCode: 403,
      status: statuses[403],
      message: missedParamsMessage || 'Wrong google access token',
    },
    [ACCESS_RESTRICTED]: {
      statusCode: 403,
      status: statuses[403],
      message: 'Access forbidden',
    },
    [CUSTOM_ERROR]: {
      statusCode: errorDetails.code,
      status: statuses[errorDetails.code],
      message: errorDetails.message,
    },
  };

  return { hasError: true, ...errorObject[errorConst] };
};

export default (errorConst, missedParams) =>
  errorConstructor(errorConst, missedParams);
