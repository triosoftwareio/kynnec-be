import btoa from 'btoa';
import Helpers from '../../shared/utils';
import { makeRequest, migrateBeacons } from '../../services/api/googleApi';
import { beaconsLocalAPI } from '../../services/api/localApi';
import {
  FETCH_BEACONS_LIST,
  FETCH_BEACON,
  DELETE_BEACON,
  REGISTER_BEACON,
  ACTIVATE_BEACON,
  DEACTIVATE_BEACON,
  DECOMMISSION_BEACON,
  CREATE_ATTACHMENT,
  DELETE_ATTACHMENT,
  FETCH_ATTACHMENT,
} from '../../shared/constants';

const { generateCustomResponse } = Helpers;

export const getBeaconsListHandler = async (req, res) => {
  const { params } = req;
  try {
    const beaconsListResponse = await makeRequest(FETCH_BEACONS_LIST, {
      params: Object.assign({}, params, { pageSize: 1000 }),
    });
    const error = beaconsListResponse.hasError ? beaconsListResponse : null;
    const responseBody = beaconsListResponse.hasError
      ? {}
      : { beacons: beaconsListResponse };

    generateCustomResponse(
      beaconsListResponse.statusCode || 200,
      error,
      responseBody,
      '',
      res,
    );
  } catch (error) {
    console.log(error);
  }
};

export const getBeaconHandler = async (req, res) => {
  const { params } = req;
  try {
    const oneBeaconResponse = await makeRequest(FETCH_BEACON, { params });
    const error = oneBeaconResponse.hasError ? oneBeaconResponse : null;
    const responseBody = oneBeaconResponse.hasError
      ? {}
      : { beacon: oneBeaconResponse };

    generateCustomResponse(
      oneBeaconResponse.statusCode || 200,
      error,
      responseBody,
      '',
      res,
    );
  } catch (error) {
    console.log('err', error);
  }
};

export const activateBeaconHandler = async (req, res) => {
  const { params } = req;
  try {
    const activateBeaconResponse = await makeRequest(ACTIVATE_BEACON, {
      params,
    });
    const error = activateBeaconResponse.hasError
      ? activateBeaconResponse
      : null;
    const message = error ? error.message : 'Success';

    generateCustomResponse(
      activateBeaconResponse.statusCode,
      error,
      {},
      message,
      res,
    );
  } catch (error) {
    console.log(error);
  }
};

export const deactivateBeaconHandler = async (req, res) => {
  const { params } = req;
  try {
    const deactivateBeaconResponse = await makeRequest(DEACTIVATE_BEACON, {
      params,
    });
    const error = deactivateBeaconResponse.hasError
      ? deactivateBeaconResponse
      : null;
    const message = error ? error.message : 'Success';

    generateCustomResponse(
      deactivateBeaconResponse.statusCode || 200,
      error,
      {},
      message,
      res,
    );
  } catch (error) {
    console.log(error);
  }
};

export const decommissionBeaconHandler = async (req, res) => {
  const { params } = req;
  try {
    const decommissionBeaconResponse = await makeRequest(DECOMMISSION_BEACON, {
      params,
    });
    const error = decommissionBeaconResponse.hasError
      ? decommissionBeaconResponse
      : null;
    const message = error ? error.message : 'Success';

    generateCustomResponse(
      decommissionBeaconResponse.statusCode || 200,
      error,
      {},
      message,
      res,
    );
  } catch (error) {
    console.log(error);
  }
};

export const deleteBeaconHandler = async (req, res) => {
  const { params } = req;
  try {
    const deleteBeaconResponse = await makeRequest(DELETE_BEACON, { params });
    const error = deleteBeaconResponse.hasError ? deleteBeaconResponse : null;
    const message = error ? error.message : 'Success';

    generateCustomResponse(
      deleteBeaconResponse.statusCode || 200,
      error,
      {},
      message,
      res,
    );
  } catch (error) {
    console.log(error);
  }
};

export const registerBeaconHandler = async () => {
  const token =
    'ya29.ElrHBEHxJApF8RJ4SevHYQS89doveaIew3NdIE8jKns0cxV8IxZognzJOuSUyvcbTsYagmC-yIbb_-jxjDHTiLOLZsN9BOJIruspq632dpS851FA3OEUxZzxPWk';
  const body = {};
  const beaconRegisterResponse = await makeRequest(REGISTER_BEACON, {
    token,
    body,
  });

  console.log(beaconRegisterResponse);
};

export const createAttachmentHandler = async (req, res) => {
  const { params, body } = req;

  try {
    const { title, url, targeting } = body;
    // const attachmentBody = {"title":"bodka","url":"https://google.com","targeting":[{"startDate":"2017-09-26","endDate":"2017-09-30","startTimeOfDay":"10:00","endTimeOfDay":"17:00"}]};
    const attachmentData = {
      namespacedType: 'com.google.nearby/en',
      data: btoa(JSON.stringify({ title, url, targeting })),
    };

    const createAttachmentResponse = await makeRequest(CREATE_ATTACHMENT, {
      body: attachmentData,
      params,
    });

    const error = createAttachmentResponse.hasError
      ? createAttachmentResponse
      : null;
    const message = error ? error.message : 'Success';
    const responseBody = createAttachmentResponse.hasError
      ? {}
      : { attachment: createAttachmentResponse };

    generateCustomResponse(
      createAttachmentResponse.statusCode || 200,
      error,
      responseBody,
      message,
      res,
    );
  } catch (error) {
    console.log(error);
  }
};

export const deleteAttachmentHandler = async (req, res) => {
  const { params } = req;
  try {
    const deleteAttachmentResponse = await makeRequest(DELETE_ATTACHMENT, {
      params,
    });
    const error = deleteAttachmentResponse.hasError
      ? deleteAttachmentResponse
      : null;
    const message = error ? error.message : 'Success';

    generateCustomResponse(
      deleteAttachmentResponse.statusCode || 200,
      error,
      {},
      message,
      res,
    );
  } catch (error) {
    console.log(error);
  }
};

export const getAttachmentListHandler = async (req, res) => {
  const { params } = req;
  try {
    const attachmentListResponse = await makeRequest(FETCH_ATTACHMENT, {
      params,
    });

    const error = attachmentListResponse.hasError
      ? attachmentListResponse
      : null;
    const message = error ? error.message : 'Success';

    generateCustomResponse(
      attachmentListResponse.statusCode || 200,
      error,
      { attachments: attachmentListResponse },
      message,
      res,
    );
  } catch (error) {
    console.log(error);
  }
};

// Local data handlers

export const migrateBeaconsHandler = async (req, res) => {
  try {
    const beaconsToMigrate = req.body.beaconsIds || null;
    const projectFrom = req.body.projectFrom || null;
    const projectTo = req.body.projectTo;

    if (!projectTo || !beaconsToMigrate || !projectFrom) return;

    const migratedBeacons = migrateBeacons(
      projectFrom,
      beaconsToMigrate,
      projectTo,
    );

    const localUpdateResult = await beaconsLocalAPI.updateProjectId(
      migratedBeacons.map(b => !!b),
      projectTo,
    );

    generateCustomResponse(200, null, { result: localUpdateResult }, '', res);
  } catch (error) {
    console.log(error);
  }
};
