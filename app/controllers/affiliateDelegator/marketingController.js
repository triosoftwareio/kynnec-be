import {
  generateReport,
  getUserDownline,
} from '../../services/api/marketingApi';
import Helpers from '../../shared/utils';
import errorHandler from './../errorHandler';
import { CUSTOM_ERROR, SOMETHING_GOES_WRONG } from '../../shared/constants';

const { generateResponse } = Helpers;

export const calculateCommissions = async (req, res) => {
  try {
    const {
      email,
      payoutEndDate,
      cashBonusStartDate,
      cashBonusEndDate,
    } = req.body;

    const getUserReport = await generateReport(
      email || 'takelifetothemax@gmail.com',
      payoutEndDate,
      cashBonusStartDate,
      cashBonusEndDate,
    );

    const userDownline = await getUserDownline(
      email || 'takelifetothemax@gmail.com',
      payoutEndDate,
    );

    if (getUserReport.hasError || userDownline.hasError)
      throw errorHandler(SOMETHING_GOES_WRONG);

    generateResponse({ responseInfo: { getUserReport, userDownline }, res });
  } catch (e) {
    let erHndl = e;
    if (!erHndl.hasError) {
      erHndl = errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: e,
      });
    }
    generateResponse({ responseInfo: erHndl, res });
  }
};

export const calculateSuperCommissions = async () => {
  // Adding Code Later
};
