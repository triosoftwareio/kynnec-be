import Helpers from '../../shared/utils';
import {
  CUSTOM_ERROR,
  CURRENCY,
  REVENUE_SCHEDULE_TYPE,
  ERROR_STATUS,
  SUBSCRIPTION_TYPE,
  SUBSCRIPTION_ADON_TYPE,
  SUBSCRIPTION_CANCELED_STATUS,
  PRIORITY_DOMESTIC_SHIPMENT,
  PRIORITY_INTERNATIONAL_SHIPMENT,
  REGULAR_DOMESTIC_SHIPMENT,
  REGULAR_INTERNATIONAL_SHIPMENT,
  LASTMONTH_CHARGE_NAME,
  CUSTOM_RECURLY_ERROR,
  EXPIRED_STATUS,
  RECURLY_ADDON_PRICE,
  RECURLY_CONVERT_IN_CENT,
} from '../../shared/constants';
import errorHandler from './../errorHandler';
import recApi from '../../services/api/recurlyApi';
import { registerUser, prepareFEUserModel } from '../../services/auth';

const { generateResponse } = Helpers;

const formatDataForRecurly = (params, type) => {
  const resultData = {};
  switch (type) {
    case 'accountData':
      resultData.account_code = params.emailAddress;
      resultData.username = params.emailAddress;
      resultData.email = params.emailAddress;
      resultData.first_name = params.firstName;
      resultData.last_name = params.lastName;
      resultData.address = {
        phone: params.phoneNumber,
        country: params.country,
        address1: params.shippingAdress1,
        address2: params.shippingAdress2,
        zip: params.postalCode,
        state: params.state,
      };
      resultData.billing_info = {
        token_id: params.recurlyToken,
      };
      break;
    case 'chargeParams':
      resultData.currency = CURRENCY;
      resultData.unit_amount_in_cents = params.price;
      resultData.description = params.name;
      resultData.revenue_schedule_type = REVENUE_SCHEDULE_TYPE;
      resultData.tax_exempt = false;
      break;
    default:
      break;
  }
  return resultData;
};

const generatePostponeDate = () => {
  const currentDate = new Date();
  const currentDayOfMonth = currentDate.getDate();
  if (currentDayOfMonth <= 10) {
    currentDate.setDate(5);
  } else if (currentDayOfMonth > 10 && currentDayOfMonth <= 20) {
    currentDate.setDate(12);
  } else {
    currentDate.setDate(19);
  }
  currentDate.setMonth(currentDate.getMonth() + 1);
  return currentDate.toISOString();
};

const getUserAccount = async (email, params) => {
  try {
    let account = await recApi.getAccount(email);
    const accountDetails = formatDataForRecurly(params, 'accountData');

    if (account.data === 404) {
      account = await recApi.createAccount(accountDetails);
    } else {
      account = await recApi.updateAccount(email, accountDetails);
    }
    if (account.status === ERROR_STATUS) {
      throw errorHandler(CUSTOM_ERROR, {
        code: account.data,
        message: account.additional.error
          ? account.additional.error.description
          : CUSTOM_RECURLY_ERROR,
      });
    }
    return account;
  } catch (e) {
    if (!e.hasError) {
      console.log(e);
      return errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: e,
      });
    }
    return e;
  }
};

const createCharge = async (email, params, subscription) => {
  try {
    const chargeParams = formatDataForRecurly(params, 'chargeParams');
    if (subscription && chargeParams.description === LASTMONTH_CHARGE_NAME) {
      chargeParams.unit_amount_in_cents =
        params.quantity * RECURLY_ADDON_PRICE * RECURLY_CONVERT_IN_CENT;
    }

    const charge = await recApi.createCharge(email, chargeParams);

    if (charge.status === ERROR_STATUS) {
      throw errorHandler(CUSTOM_ERROR, {
        code: charge.data,
        message: charge.additional.error
          ? charge.additional.error.description
          : CUSTOM_RECURLY_ERROR,
      });
    }
    return charge;
  } catch (e) {
    if (!e.hasError) {
      console.log(e);
      return errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: e,
      });
    }
    return e;
  }
};

const getSubscription = async email => {
  try {
    const subscriptions = await recApi.getSubsList(email, {});
    if (subscriptions.status === ERROR_STATUS) {
      throw errorHandler(CUSTOM_ERROR, {
        code: subscriptions.data,
        message: subscriptions.additional.error
          ? subscriptions.additional.error.description
          : CUSTOM_RECURLY_ERROR,
      });
    }

    if (
      subscriptions.data.subscriptions.subscription &&
      subscriptions.data.subscriptions.subscription.length
    ) {
      const filteredSubscription = subscriptions.data.subscriptions.subscription.filter(
        el => el.plan.plan_code === SUBSCRIPTION_TYPE,
      );
      return filteredSubscription[0].state !== EXPIRED_STATUS
        ? filteredSubscription[0]
        : false;
    }

    return subscriptions.data.subscriptions.subscription &&
    subscriptions.data.subscriptions.subscription.state !== EXPIRED_STATUS
      ? subscriptions.data.subscriptions.subscription
      : false;
  } catch (e) {
    if (!e.hasError) {
      console.log(e);
      return errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: e,
      });
    }
    return e;
  }
};

const updateExistingSubscription = async (subscription, params) => {
  try {
    const { uuid } = subscription;

    if (subscription.state === SUBSCRIPTION_CANCELED_STATUS) {
      const activateSubs = await recApi.reActivateSub(uuid);

      if (activateSubs.status === ERROR_STATUS) {
        throw errorHandler(CUSTOM_ERROR, {
          code: activateSubs.data,
          message: activateSubs.additional.error
            ? activateSubs.additional.error.description
            : CUSTOM_RECURLY_ERROR,
        });
      }
    }
    let quantity = parseInt(params.charge.beaconQuantity, 10);
    if (subscription.subscription_add_ons.subscription_add_on) {
      quantity += parseInt(
        subscription.subscription_add_ons.subscription_add_on.quantity._,
        10,
      );
    }
    const subscriptionData = {
      plan_code: SUBSCRIPTION_TYPE,
      timeframe: 'now',
      subscription_add_ons: [
        {
          subscription_add_on: {
            add_on_code: SUBSCRIPTION_ADON_TYPE,
            quantity,
          },
        },
      ],
    };
    const newSub = await recApi.updateSubscription(uuid, subscriptionData);
    if (newSub.status === ERROR_STATUS) {
      throw errorHandler(CUSTOM_ERROR, {
        code: newSub.data,
        message: newSub.additional.error
          ? newSub.additional.error.description
          : CUSTOM_RECURLY_ERROR,
      });
    }
    return newSub;
  } catch (e) {
    if (!e.hasError) {
      console.log(e);
      return errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: e,
      });
    }
    return e;
  }
};

const createNewSubscription = async params => {
  try {
    const accountDetails = formatDataForRecurly(params, 'accountData');
    const subAddon = {};

    if (params.charge.beaconQuantity > 1) {
      subAddon.subscription_add_on = {};
      subAddon.subscription_add_on.quantity = params.charge.beaconQuantity - 1;
      subAddon.subscription_add_on.add_on_code = SUBSCRIPTION_ADON_TYPE;
    }

    const subscriptionData = {
      plan_code: SUBSCRIPTION_TYPE,
      currency: CURRENCY,
      account: accountDetails,
      quantity: 1,
      subscription_add_ons: subAddon,
    };

    const newSub = await recApi.createSubscription(subscriptionData);

    if (newSub.status !== ERROR_STATUS) {
      const postPoneSub = await recApi.postponeSub(
        newSub.data.subscription.uuid,
        generatePostponeDate(),
      );

      if (postPoneSub.status === ERROR_STATUS) {
        throw errorHandler(CUSTOM_ERROR, {
          code: newSub.data,
          message: postPoneSub.additional.error
            ? postPoneSub.additional.error.description
            : CUSTOM_RECURLY_ERROR,
        });
      }
    }

    if (newSub.status === ERROR_STATUS) {
      throw errorHandler(CUSTOM_ERROR, {
        code: newSub.data,
        message: newSub.additional.error
          ? newSub.additional.error.description
          : CUSTOM_RECURLY_ERROR,
      });
    }

    return newSub;
  } catch (e) {
    if (!e.hasError) {
      console.log(e);
      return errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: e,
      });
    }
    return e;
  }
};

const updateSubscription = async (email, params, subscription) => {
  try {
    let newSub = {};

    if (subscription) {
      newSub = await updateExistingSubscription(subscription, params);

      if (newSub.hasError) throw newSub;

      return newSub;
    }
    newSub = await createNewSubscription(params);

    if (newSub.hasError) throw newSub;

    return newSub;
  } catch (e) {
    if (!e.hasError) {
      console.log(e);
      return errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: e,
      });
    }
    return e;
  }
};

const updateUserInformation = async (params, subscription) => {
  try {
    const subscriptionAddons = {};
    if (
      subscription.data.subscription.subscription_add_ons &&
      subscription.data.subscription.subscription_add_ons.subscription_add_on
    ) {
      subscriptionAddons.code =
        subscription.data.subscription.subscription_add_ons.subscription_add_on.add_on_code;
      subscriptionAddons.price =
        subscription.data.subscription.subscription_add_ons.subscription_add_on.unit_amount_in_cents._;
      subscriptionAddons.quantity =
        subscription.data.subscription.subscription_add_ons.subscription_add_on.quantity._;
    }

    const userInformation = {
      firstName: params.firstName,
      lastName: params.lastName,
      phoneNumber: params.phoneNumber,
      email: params.emailAddress,
      country: params.country,
      affiliate_id: null,
      referredBy: params.referredBy,
      afiliate: params.afiliate,
      address: params.shippingAdress1,
      city: params.city,
      zip: params.postalCode,
      state: params.state,
      unassignedBeacons:
        parseInt(subscription.data.subscription.quantity._, 10) +
        parseInt(subscriptionAddons.quantity || 0, 10),
      subscription: {
        id: subscription.data.subscription.uuid,
        currency: subscription.data.subscription.currency,
        state: subscription.data.subscription.state,
        quantity:
          parseInt(subscription.data.subscription.quantity._, 10) +
          parseInt(subscriptionAddons.quantity || 0, 10),
        periodStart: subscription.data.subscription.current_period_started_at._,
        periodEnd: subscription.data.subscription.current_period_ends_at._,
        price: subscription.data.subscription.unit_amount_in_cents._,
        subscription_add_ons: subscriptionAddons,
      },
      password: params.password,
    };

    const { savedSystemUser: user, newCustomer } = await registerUser(
      userInformation,
    );
    user.customer = newCustomer;

    return user;
  } catch (e) {
    if (!e.hasError) {
      return errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: e,
      });
    }
    return e;
  }
};

const deleteCharge = async (deliveryCharge, chargeLastMonth) => {
  try {
    const delDelivery = await recApi.deleteCharge(deliveryCharge);

    if (delDelivery.status === ERROR_STATUS) {
      throw errorHandler(CUSTOM_ERROR, {
        code: delDelivery.data,
        message: delDelivery.additional.error
          ? delDelivery.additional.error.description
          : CUSTOM_RECURLY_ERROR,
      });
    }
    const delLastMonth = await recApi.deleteCharge(chargeLastMonth);

    if (delLastMonth.status === ERROR_STATUS) {
      throw errorHandler(CUSTOM_ERROR, {
        code: delLastMonth.data,
        message: delLastMonth.additional.error
          ? delLastMonth.additional.error.description
          : CUSTOM_RECURLY_ERROR,
      });
    }

    return true;
  } catch (e) {
    if (!e.hasError) {
      console.log(e);
      return errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: e,
      });
    }
    return e;
  }
};

/* eslint-disable */
export const createSubscription = async (req, res) => {
  /* eslint-enable */
  const { body } = req;
  const { emailAddress } = body;

  try {
    const account = await getUserAccount(emailAddress, body);

    if (account.hasError) throw account;

    const currentActiveSubscription = await getSubscription(emailAddress);

    if (currentActiveSubscription.hasError) throw currentActiveSubscription;

    const deliveryCharge = {
      price: body.charge.shippingPrice * RECURLY_CONVERT_IN_CENT,
    };

    switch (body.charge.shippingPrice) {
      case 30:
        deliveryCharge.name = REGULAR_INTERNATIONAL_SHIPMENT;
        break;
      case 40:
        deliveryCharge.name = PRIORITY_DOMESTIC_SHIPMENT;
        break;
      case 80:
        deliveryCharge.name = PRIORITY_INTERNATIONAL_SHIPMENT;
        break;
      default:
        deliveryCharge.name = REGULAR_DOMESTIC_SHIPMENT;
        break;
    }

    const chargeDelivery = await createCharge(emailAddress, deliveryCharge);

    if (chargeDelivery.hasError) throw chargeDelivery;

    const latsMonthCharge = {
      price: body.charge.lastMonthPayment * RECURLY_CONVERT_IN_CENT,
      name: LASTMONTH_CHARGE_NAME,
      quantity: body.charge.beaconQuantity,
    };

    const chargeLastMonth = await createCharge(
      emailAddress,
      latsMonthCharge,
      currentActiveSubscription,
    );

    if (chargeLastMonth.hasError) throw chargeLastMonth;

    const currentSubscription = await updateSubscription(
      emailAddress,
      body,
      currentActiveSubscription,
    );

    if (currentSubscription.hasError) {
      const chargeDelete = await deleteCharge(
        chargeDelivery.data.adjustment.uuid,
        chargeLastMonth.data.adjustment.uuid,
      );
      if (chargeDelete.hasError) throw chargeDelete;
      throw currentSubscription;
    }

    const user = await updateUserInformation(body, currentSubscription);

    generateResponse({ responseInfo: prepareFEUserModel(user), res });
  } catch (e) {
    let erHndl = e;
    if (!erHndl.hasError) {
      console.log(e);
      erHndl = errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: e,
      });
    }
    generateResponse({ responseInfo: erHndl, res });
  }
};

export const lookUpAccount = async (req, res) => {
  try {
    const { email } = req.query;
    if (!email) {
      throw errorHandler(CUSTOM_ERROR, {
        code: 422,
        message: 'Missing parameter',
      });
    }

    const account = await recApi.getAccount(email);
    if (account.status === ERROR_STATUS) {
      throw errorHandler(CUSTOM_ERROR, {
        code: account.data,
        message: account.additional.error
          ? account.additional.error.description
          : CUSTOM_RECURLY_ERROR,
      });
    }
    generateResponse({ responseInfo: account.data.account, res });
  } catch (e) {
    let erHndl = e;
    if (!erHndl.hasError) {
      console.log(e);
      erHndl = errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: e,
      });
    }
    generateResponse({ responseInfo: erHndl, res });
  }
};
