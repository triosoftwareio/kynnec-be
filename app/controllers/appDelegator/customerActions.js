import Helpers from '../../shared/utils';
import errorHandler from './../errorHandler';
import {
  beaconsLocalAPI,
  customersLocalAPI,
} from '../../services/api/localApi';
import { updateAttachment } from '../../services/api/googleApi';
import {
  SOMETHING_GOES_WRONG,
  ACCESS_RESTRICTED,
  MISSED_PARAMS,
  CUSTOM_ERROR,
} from '../../shared/constants';

const { generateResponse, generateCustomResponse } = Helpers;

// parseCsvFile();

export const getCustomerBeaconsListHandler = async (req, res) => {
  const customer = req.user.customer || req.user;
  const findQuery = { customer: customer._id };

  const beaconsListResponse = await beaconsLocalAPI.beaconsListDataTable(req, {
    find: findQuery,
  });

  generateResponse({ responseInfo: beaconsListResponse, res });
};

export const getCustomerBeaconHandler = async (req, res) => {
  const customer = req.user.customer || req.user;
  const { beaconId } = req.params;
  const findQuery = { customer: customer._id };
  findQuery._id = beaconId;

  const customerBeaconResponse = await beaconsLocalAPI.getOne(findQuery);

  generateResponse({ responseInfo: customerBeaconResponse, res });
};

export const updateCustomerInfo = async (req, res) => {
  const currentCustomer = req.user.customer || req.user;
  const { customerId } = req.params;
  const { customer } = req.body;

  if (currentCustomer.id !== customerId) {
    const error = errorHandler(ACCESS_RESTRICTED);
    generateResponse({ responseInfo: error, res });
    return;
  }

  const updateCustomerResponse = await customersLocalAPI.update(
    { _id: customerId },
    customer,
  );

  generateResponse({
    responseInfo: updateCustomerResponse,
    successMessage: 'Customer successfully updated',
    res,
  });
};

export const updateCustomerAffiliateAdjustment = async (req, res) => {
  const { customerId } = req.params;
  const { customer } = req.body;

  if (!customerId) {
    const error = errorHandler(MISSED_PARAMS);
    generateResponse({ responseInfo: error, res });
    return;
  }

  const updateCustomerResponse = await customersLocalAPI.update(
    { _id: customerId },
    customer,
  );

  generateResponse({
    responseInfo: updateCustomerResponse,
    successMessage: 'Customer successfully updated',
    res,
  });
};

export const updateBeaconMessageRequestHandler = async (req, res) => {
  const customer = req.user.customer || req.user;
  const { id: beaconId, attachmentName } = req.params;
  const { attachment } = req.body;

  try {
    const localBeacon = await beaconsLocalAPI
      .getOne({ _id: beaconId, customer: customer._id })
      .populate('project');

    const { projectId } = localBeacon.project;
    const beaconName = localBeacon.beaconName.split('/')[1];

    const apiUpdateBeaconResponse = await updateAttachment(
      projectId,
      beaconName,
      attachmentName,
      attachment,
    );

    if (apiUpdateBeaconResponse.hasError) {
      generateResponse({ responseInfo: apiUpdateBeaconResponse, res });
      return;
    }

    const updateRequestResponse = await beaconsLocalAPI.updateBeaconAttachmentRequest(
      beaconId,
      customer._id,
      apiUpdateBeaconResponse,
    );

    generateResponse({
      responseInfo: updateRequestResponse,
      successMessage: 'Attachments successfully updated',
      res,
    });
  } catch (error) {
    console.log(error);
    generateCustomResponse(
      500,
      errorHandler(SOMETHING_GOES_WRONG),
      null,
      null,
      res,
    );
  }
};

export const changeAffiliateCodeHandler = async (req, res) => {
  const customer = req.user.customer || req.user;
  const { affiliateCode } = req.body;

  const isAffiliateCodeExist = await customersLocalAPI.getOne({
    $or: [{ affiliateCode }, { referredBy: affiliateCode }],
  });

  if (!customer.affiliateCode || isAffiliateCodeExist) {
    generateResponse({
      responseInfo: errorHandler(CUSTOM_ERROR, {
        code: 422,
        message: 'Affiliate code already exist. Please, try, later',
      }),
      res,
    });
    return;
  }

  const updateCustomerResult = await customersLocalAPI.update(
    { _id: customer._id },
    { affiliateCode },
  );

  generateResponse({
    responseInfo: updateCustomerResult,
    successMessage: 'Affiliate code successfully updated',
    res,
  });
};

// export const updateDatabase = () => {
//   // prepareDatabaseData();
//   // prepareDatabaseBeacons();
// };
