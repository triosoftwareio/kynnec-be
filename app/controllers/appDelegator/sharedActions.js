import Helpers from '../../shared/utils';
import errorHandler from './../errorHandler';
import {
  authMiddleware,
  checkAccessRights,
  checkMobileAccess,
  registerUser,
  authenticate,
  logout,
  generateRestorePasswordLink,
  changeUserPassword,
  prepareFEUserModel,
} from '../../services/auth';

import { beaconsLocalAPI } from '../../services/api/localApi';

import { SOMETHING_GOES_WRONG } from '../../shared/constants';

const { generateResponse, generateCustomResponse } = Helpers;

export const authMiddlewareHandler = (...rest) => authMiddleware(...rest);
export const checkAccessRightsHandler = (...rest) => checkAccessRights(...rest);
export const checkMobileAccessRightHandler = (...rest) =>
  checkMobileAccess(...rest);

export const getCustomerByToken = async (req, res) => {
  try {
    const userResponse = req.user.customer
      ? prepareFEUserModel(req.user)
      : req.user;
    generateResponse({ responseInfo: userResponse, res });
  } catch (error) {
    console.log(error);
    generateCustomResponse(
      500,
      errorHandler(SOMETHING_GOES_WRONG),
      null,
      null,
      res,
    );
  }
};

export const authenticateUserHandler = async (req, res) => {
  const { login, password } = req.body;
  const authResponse = await authenticate(login, password);

  generateResponse({ responseInfo: authResponse, res });
};

export const registerUserHandler = async (req, res) => {
  try {
    const { customerInfo, orderInfo } = req.body;
    const registerUserResponse = await registerUser(customerInfo, orderInfo);

    if (registerUserResponse.hasError) {
      generateResponse({ responseInfo: registerUserResponse, res });
      return;
    }

    const error = Object.values(registerUserResponse).some(r => r.hasError)
      ? registerUserResponse
      : null;
    const responseBody = error ? {} : registerUserResponse;

    generateCustomResponse(error ? 500 : 200, responseBody, {}, '', res);
  } catch (error) {
    console.log(error);
  }
};

export const logoutUserHandler = async (req, res) => {
  const { token } = req.user;
  const logoutResponse = await logout(token);

  generateResponse({ responseInfo: logoutResponse, res });
};

export const restorePasswordHandler = async (req, res) => {
  const { login } = req.body;

  const restoreHashGeneratingResponse = await generateRestorePasswordLink(
    login,
  );
  generateResponse({
    responseInfo: restoreHashGeneratingResponse,
    successMessage: 'Link successfully sent on email',
    res,
  });
};

export const changePasswordHandler = async (req, res) => {
  const { restoreHash, password } = req.body;

  const changePasswordResponse = await changeUserPassword(
    restoreHash,
    password,
  );
  generateResponse({
    responseInfo: changePasswordResponse,
    successMessage:
      'Password successfully changed. Now you can log in with new credentials',
    res,
  });
};

export const searchOneBeaconHandler = async (req, res) => {
  const { beaconName } = req.query;
  const firstRegexp = new RegExp(`.*(R - ${beaconName})$`, 'ig');
  const secondRegexp = new RegExp(
    `.*(@[a-z0-9]{1,20}\.[a-z]{2,5}[\s]-[\s]${beaconName})`, //eslint-disable-line
    'ig',
  );

  const oneBeaconResponse = await beaconsLocalAPI.getWithPopulate({
    $or: [{ description: firstRegexp }, { description: secondRegexp }],
  });

  if (!oneBeaconResponse) {
    const updatedBeaconResponse = await beaconsLocalAPI.updateBeaconFromApi(
      beaconName,
    );
    generateResponse({ responseInfo: updatedBeaconResponse, res });
    return;
  }

  generateResponse({ responseInfo: oneBeaconResponse, res });
};
