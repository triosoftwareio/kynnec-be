import Helpers from '../../shared/utils';
import errorHandler from './../errorHandler';
import {
  beaconsLocalAPI,
  accountsLocalAPI,
  projectLocalAPI,
  customersLocalAPI,
} from '../../services/api/localApi';
import { makeRequest, updateAttachment } from '../../services/api/googleApi';
import { impersonate } from '../../services/auth';
import {
  MISSED_PARAMS,
  DECOMMISSION_BEACON,
  SOMETHING_GOES_WRONG,
  CUSTOMER_ORDER_STATUS_IN_PROGRESS,
  CUSTOMER_ORDER_STATUS_DONE,
  ORDER_SHIPPED_STATUS,
  ACCESS_RESTRICTED,
} from '../../shared/constants';

const { generateResponse, generateCustomResponse } = Helpers;

export const createAccountHandler = async (req, res) => {
  const accountData = req.body.account || null;
  const accountResponse = await accountsLocalAPI.create(accountData);

  generateResponse({ responseInfo: accountResponse, res });
};

export const accountsListHandler = async (req, res) => {
  const accountsListResponse = await accountsLocalAPI.list({});

  generateResponse({ responseInfo: accountsListResponse, res });
};

export const createProjectHandler = async (req, res) => {
  try {
    const accountId = req.params.accountId || null;
    const projectData = req.body.project || null;

    const selectedAccount = await accountsLocalAPI.getById(accountId);

    if (!selectedAccount || selectedAccount.hasError) {
      const error = errorHandler(MISSED_PARAMS);
      generateResponse({ responseInfo: error, res });
      return;
    }

    projectData.account = selectedAccount._id;
    const newProjectResponse = await projectLocalAPI.create(projectData);

    generateResponse({ responseInfo: newProjectResponse, res });
  } catch (error) {
    console.log(error);
  }
};

export const getOneProjectHandler = async (req, res) => {
  const { projectId } = req.params;
  const oneProjectResponse = await projectLocalAPI.getById(projectId);

  generateResponse({ responseInfo: oneProjectResponse, res });
};

export const updateProjectHandler = async (req, res) => {
  try {
    const projectId = req.body.projectId || null;
    const { purpose } = req.body;

    const updateProjectResponse = await projectLocalAPI.update(
      { _id: projectId },
      { purpose },
    );

    generateResponse({
      responseInfo: updateProjectResponse,
      res,
      successMessage: 'Project successfully updated',
    });
  } catch (error) {
    console.log(error);
  }
};

export const blockProjectHandler = async (req, res) => {
  try {
    const projectId = req.body.projectId || null;

    const updateProjectResponse = await projectLocalAPI.update(
      { _id: projectId },
      { isBlockedByGoogle: true },
    );

    generateResponse({
      responseInfo: updateProjectResponse,
      res,
      successMessage: 'Project successfully blocked',
    });
  } catch (error) {
    console.log(error);
  }
};

export const projectsListHandler = async (req, res) => {
  const beaconsListResponse = await projectLocalAPI.projectsListDataTable(req, {
    find: {},
  });

  generateResponse({ responseInfo: beaconsListResponse, res });
};

export const beaconsListHandler = async (req, res) => {
  // TODO : check this if for search customers
  const beaconsListResponse = await beaconsLocalAPI.beaconsListDataTable(req);

  generateResponse({ responseInfo: beaconsListResponse, res });
};

export const attachmentsRequests = async (req, res) => {
  const beaconsListResponse = await beaconsLocalAPI.list({
    attachmentsRequest: { $exists: true, $ne: [] },
  });

  generateResponse({ responseInfo: beaconsListResponse, res });
};

export const oneBeaconHandler = async (req, res) => {
  const { beaconId } = req.params;
  const oneBeaconResponse = await beaconsLocalAPI.getWithPopulate({
    _id: beaconId,
  });

  generateResponse({ responseInfo: oneBeaconResponse, res });
};

export const changeBeaconStatusHandler = async (req, res) => {
  const { beacons: beaconsToUpdate, status } = req.body;

  const updatedBeaconsResponse = beaconsToUpdate.filter(async beaconObject => {
    const params = {
      name: beaconObject.beaconName,
      projectId: beaconObject.projectId,
    };

    const updateBeaconStatusResponse = await beaconsLocalAPI.changeBeaconStatus(
      status,
      { params },
    );
    return !updateBeaconStatusResponse.hasError;
  });

  generateResponse({ responseInfo: updatedBeaconsResponse, res });
};

export const activateBeaconsHandler = async (req, res) => {
  const { beaconName, projectId } = req.body;
  const { user, isMobileAction } = req;
  const { beaconId } = req.params;

  if (
    isMobileAction &&
    !await beaconsLocalAPI.getOne({ _id: beaconId, customer: user._id })
  ) {
    generateResponse({ responseInfo: errorHandler(ACCESS_RESTRICTED), res });
    return;
  }

  const status = 'active';
  const params = {
    name: beaconName,
    projectId,
  };

  const activateBeaconResponse = await beaconsLocalAPI.changeBeaconStatus(
    status,
    params,
  );

  generateResponse({ responseInfo: activateBeaconResponse, res });
};

export const deactivateBeaconsHandler = async (req, res) => {
  const { beaconName, projectId } = req.body;
  const { beaconId } = req.params;
  const { user, isMobileAction } = req;

  if (
    isMobileAction &&
    !await beaconsLocalAPI.getOne({ _id: beaconId, customer: user._id })
  ) {
    generateResponse({ responseInfo: errorHandler(ACCESS_RESTRICTED), res });
    return;
  }

  const status = 'inactive';
  const params = {
    name: beaconName,
    projectId,
  };

  const deactivateBeaconResponse = await beaconsLocalAPI.changeBeaconStatus(
    status,
    params,
  );

  generateResponse({ responseInfo: deactivateBeaconResponse, res });
};

export const decommissionBeaconsHandler = async (req, res) => {
  const { beaconId } = req.params;
  const { beaconName, projectId } = req.body;
  const { user, isMobileAction } = req;

  if (
    isMobileAction &&
    !await beaconsLocalAPI.getOne({ _id: beaconId, customer: user._id })
  ) {
    generateResponse({ responseInfo: errorHandler(ACCESS_RESTRICTED), res });
    return;
  }

  const decommissionApiResponse = await makeRequest(DECOMMISSION_BEACON, {
    params: {
      name: beaconName,
      projectId,
    },
  });

  if (decommissionApiResponse.hasError) {
    generateCustomResponse(
      500,
      errorHandler(SOMETHING_GOES_WRONG),
      null,
      null,
      res,
    );
    return;
  }

  const decommissionBeaconResponse = await beaconsLocalAPI.remove({
    _id: beaconId,
  });

  generateResponse({ responseInfo: decommissionBeaconResponse, res });
};

export const approveAttachmentMessageHandler = async (req, res) => {
  const { beaconId } = req.params;

  const approveAttachmentResponse = await beaconsLocalAPI.approveBeaconAttachmentRequest(
    beaconId,
  );

  generateResponse({
    responseInfo: approveAttachmentResponse,
    res,
    successMessage: 'Attachment successfully updated',
  });
};

export const provisionBeaconHandler = async (req, res) => {
  const { beaconId } = req.params;
  const { customerId } = req.body;

  const provisionResponse = await beaconsLocalAPI.provisionBeacon(
    beaconId,
    customerId,
  );

  generateResponse({
    responseInfo: provisionResponse,
    res,
    successMessage: 'Beacons successfully provisioned',
  });
};

export const provisionedBeaconsListHandler = async (req, res) => {
  const beaconsListResponse = await beaconsLocalAPI.getProvisionedBeaconsWithCustomers();

  generateResponse({
    responseInfo: beaconsListResponse,
    res,
  });
};

export const updateProvisionedBeaconsHandler = async (req, res) => {
  const { notes, customerId, beaconsToShip } = req.body;

  const currentCustomer = await customersLocalAPI.getById(customerId);

  if (!currentCustomer || !beaconsToShip || !beaconsToShip.length) {
    generateResponse({
      responseInfo: errorHandler(MISSED_PARAMS),
      res,
    });
    return;
  }

  const customerUpdateOptions = {
    gemOrderStatus:
      currentCustomer.unassignedBeacons === 0
        ? CUSTOMER_ORDER_STATUS_DONE
        : CUSTOMER_ORDER_STATUS_IN_PROGRESS,
  };

  if (notes) customerUpdateOptions.notes = notes;

  await customersLocalAPI.update({ _id: customerId }, customerUpdateOptions);

  const beaconUpdateResponse = await beaconsLocalAPI.multiUpdate(
    { _id: { $in: beaconsToShip } },
    { orderStatus: ORDER_SHIPPED_STATUS },
  );

  generateResponse({
    responseInfo: beaconUpdateResponse,
    res,
    successMessage: 'Beacons successfully updated',
  });
};

export const orderedBeaconsHandler = async (req, res) => {
  const customersListResponse = await customersLocalAPI.customersListDataTable(
    req,
    {
      find: { gemOrderStatus: CUSTOMER_ORDER_STATUS_IN_PROGRESS },
    },
  );
  generateResponse({ responseInfo: customersListResponse, res });
};

export const updateBeaconAttachmentHandler = async (req, res) => {
  const customer = req.user.customer || req.user;
  const { beaconId, attachmentName } = req.params;
  let { attachment } = req.body;

  try {
    const localBeacon = await beaconsLocalAPI
      .getOne({ _id: beaconId, customer: customer._id })
      .populate('project');

    const { projectId } = localBeacon.project;
    const beaconName = localBeacon.beaconName.split('/')[1];

    if (!attachment) {
      const beaconAttachment = localBeacon.attachments[0];
      attachment = {
        namespacedType: beaconAttachment.namespacedType,
        ...beaconAttachment.notificationMessage,
      };
    }

    const apiUpdateBeaconResponse = await updateAttachment(
      projectId,
      beaconName,
      attachmentName,
      attachment,
    );

    if (apiUpdateBeaconResponse.hasError) {
      generateResponse({ responseInfo: apiUpdateBeaconResponse, res });
      return;
    }

    const updateRequestResponse = await beaconsLocalAPI.approveBeaconAttachmentRequest(
      beaconId,
    );

    generateResponse({
      responseInfo: updateRequestResponse,
      res,
      successMessage: 'Attachment successfully updated',
    });
  } catch (error) {
    console.log(error);
  }
};

export const customersListHandler = async (req, res) => {
  const customersListResponse = await customersLocalAPI.customersListDataTable(
    req,
  );
  generateResponse({ responseInfo: customersListResponse, res });
};

export const customersAffiliateListHandler = async (req, res) => {
  const customersListResponse = await customersLocalAPI.customersAffiliateListDataTable(
    req,
  );
  generateResponse({ responseInfo: customersListResponse, res });
};

export const impersonateHandler = async (req, res) => {
  const { customerId } = req.params;
  const { user: systemUser } = req;

  const impersonateResponse = await impersonate(customerId, systemUser);

  generateResponse({ responseInfo: impersonateResponse, res });
};
