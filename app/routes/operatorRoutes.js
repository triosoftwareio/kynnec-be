import { OPERATOR_ROLE, ADMIN_ROLE } from '../shared/constants';

import {
  checkAccessRightsHandler,
  accountsListHandler,
  createAccountHandler,
  createProjectHandler,
  getOneProjectHandler,
  updateProjectHandler,
  blockProjectHandler,
  customersListHandler,
  impersonateHandler,
  projectsListHandler,
  beaconsListHandler,
  attachmentsRequests,
  decommissionBeaconsHandler,
  activateBeaconsHandler,
  deactivateBeaconsHandler,
  oneBeaconHandler,
  approveAttachmentMessageHandler,
  provisionBeaconHandler,
  searchOneBeaconHandler,
  orderedBeaconsHandler,
  updateProvisionedBeaconsHandler,
  changeBeaconStatusHandler,
  updateBeaconAttachmentHandler,
  provisionedBeaconsListHandler,
  customersAffiliateListHandler,
  updateCustomerAffiliateAdjustment,
} from '../controllers';

export default app => {
  app.all('*', (...rest) => {
    checkAccessRightsHandler([ADMIN_ROLE, OPERATOR_ROLE], ...rest);
  });

  // Accounts routes

  app
    .route('/accounts')
    .get(accountsListHandler)
    .post(createAccountHandler);

  app
    .route('/accounts/:accountId')
    .get()
    .put();

  // Projects orders routes

  app.get('/projects-list', projectsListHandler);

  app.route('/accounts/:accountId/projects').post(createProjectHandler);

  app
    .route('/accounts/:accountId/projects/:projectId')
    .get(getOneProjectHandler)
    .put(updateProjectHandler);

  app.put(
    '/accounts/:accountId/projects/:projectId/block',
    blockProjectHandler,
  );

  app.get('/beacons', beaconsListHandler);
  app.get('/beacons/attachmentsRequests', attachmentsRequests);
  app.get('/beacons/:beaconId', oneBeaconHandler);
  app.get('/beacons-search', searchOneBeaconHandler);
  app.get('/beacons-ordered', orderedBeaconsHandler);
  app.get('/beacons-provisioned', provisionedBeaconsListHandler);
  app.put('/beacons-ship', updateProvisionedBeaconsHandler);
  app.put('/beacons/:beaconId', updateBeaconAttachmentHandler);
  app.put('/beacons/:beaconId/provision', provisionBeaconHandler);
  app.put('/beacons/:beaconId/decommission', decommissionBeaconsHandler);
  app.put('/beacons/:beaconId/activate', activateBeaconsHandler);
  app.put('/beacons/:beaconId/deactivate', deactivateBeaconsHandler);
  app.put('/beacons/bulkChangeStatus', changeBeaconStatusHandler);

  // Attachments routes

  app.put(
    '/beacons/:beaconId/approveAttachment',
    approveAttachmentMessageHandler,
  );
  app.put(
    '/beacons/:beaconId/declineAttachment',
    updateBeaconAttachmentHandler,
  );

  app.get('/customers', customersListHandler);
  app.get('/customers/affiliateList', customersAffiliateListHandler);
  app.put(
    '/customers/affiliateList/:customerId',
    updateCustomerAffiliateAdjustment,
  );
  app.get('/customers/:customerId/impersonate', impersonateHandler);

  // Super Admin routes

  app.put(
    '/move-beacons',
    (...rest) => {
      checkAccessRightsHandler([ADMIN_ROLE], ...rest);
    },
    () => {},
  );

  return app;
};
