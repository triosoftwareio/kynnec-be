import { ADMIN_ROLE } from '../shared/constants';

import {
  checkAccessRightsHandler,
  getBeaconsListHandler,
  getBeaconHandler,
  activateBeaconHandler,
  deactivateBeaconHandler,
  deleteBeaconHandler,
  decommissionBeaconHandler,
  getAttachmentListHandler,
  deleteAttachmentHandler,
  createAttachmentHandler,
} from '../controllers';

export default app => {
  app.all('*', (...rest) => {
    checkAccessRightsHandler([ADMIN_ROLE], ...rest);
  });

  app.get('/project/:projectId/beacons', getBeaconsListHandler);
  app.get('/project/:projectId/beacons/:name', getBeaconHandler);
  app.post('/project/:projectId/beacons', getBeaconsListHandler);
  app.post('/project/:projectId/beacons/:name/activate', activateBeaconHandler);
  app.post(
    '/project/:projectId/beacons/:name/deactivate',
    deactivateBeaconHandler,
  );
  app.post(
    '/project/:projectId/beacons/:name/decommission',
    decommissionBeaconHandler,
  );
  app.delete('/project/:projectId/beacons/:name', deleteBeaconHandler);
  app.get(
    '/project/:projectId/beacons/:name/attachment',
    getAttachmentListHandler,
  );
  app.delete(
    '/project/:projectId/beacons/:name/attachment/:attachmentName',
    deleteAttachmentHandler,
  );
  app.post(
    '/project/:projectId/beacons/:name/attachment',
    createAttachmentHandler,
  );

  return app;
};
