import moment from 'moment';
import log4js from 'log4js';
import Helpers from '../shared/utils';
import { registerUser } from '../services/auth';
import {
  RECURLY_WEBHOOK_SUCCESSFUL_PAYMENT,
  RECURLY_WEBHOOK_SUCCESSFUL_UPDATED,
  RECURLY_WEBHOOK_EXPIRED_SUBSCRIPTION,
  RECURLY_WEBHOOK_CANCELED_SUBSCRIPTION,
  CUSTOMER_ORDER_STATUS_IN_PROGRESS,
  CUSTOMER_ORDER_STATUS_DONE,
  MARKETING_API_KEY,
  RECURLY_WEBHOOK_NEW_SUBSCRIPTION,
} from '../shared/constants';
import recApi from '../services/api/recurlyApi';
import { customersLocalAPI } from '../services/api/localApi';
import {
  updateUser,
  addHistoricalSubscription,
  addNewUser,
} from '../services/api/marketingApi';

const logger = log4js.getLogger();
logger.level = 'debug';
const { generateResponse } = Helpers;

export default app => {
  app.post('/', async (req, res) => {
    const actionType = Object.keys(req.body)[0];

    switch (actionType) {
      case RECURLY_WEBHOOK_SUCCESSFUL_PAYMENT: {
        console.log(`${moment().format('YYYY-MM-DD H:m:s')} - ${actionType}`);
        logger.debug(`${moment().format('YYYY-MM-DD H:m:s')} - ${actionType}`);
        const price = req.body[actionType].transaction.amount_in_cents._;

        const formData = {
          email: req.body[actionType].account.email,
          status: 'Active',
          apiKey: MARKETING_API_KEY,
        };

        updateUser(formData);
        const date = moment().format('YYYY-MM-DD H:m:s');
        addHistoricalSubscription(
          req.body[actionType].account.email,
          date,
          price,
        );

        generateResponse({ responseInfo: price, res });
        break;
      }
      case RECURLY_WEBHOOK_SUCCESSFUL_UPDATED: {
        console.log(`${moment().format('YYYY-MM-DD H:m:s')} - ${actionType}`);
        logger.debug(`${moment().format('YYYY-MM-DD H:m:s')} - ${actionType}`);

        const subscriptionAddons = {};
        if (
          req.body[actionType].subscription.subscription_add_ons
            .subscription_add_on
        ) {
          subscriptionAddons.code =
            req.body[
              actionType
            ].subscription.subscription_add_ons.subscription_add_on.add_on_code;
          subscriptionAddons.price =
            req.body[
              actionType
            ].subscription.subscription_add_ons.subscription_add_on.unit_amount_in_cents._;
          subscriptionAddons.quantity =
            req.body[
              actionType
            ].subscription.subscription_add_ons.subscription_add_on.quantity._;
        }

        const quantityBeacons =
          parseInt(req.body[actionType].subscription.quantity._, 10) +
          parseInt(subscriptionAddons.quantity || 0, 10);
        const unassignedBeacons = await customersLocalAPI.hasUnassignedBeacons(
          req.body[actionType].account.email,
          quantityBeacons,
        );

        const userInformation = {
          firstName: req.body[actionType].account.first_name,
          lastName: req.body[actionType].account.last_name,
          phoneNumber: req.body[actionType].account.phone,
          email: req.body[actionType].account.email,
          gemOrderStatus: unassignedBeacons.shouldStatusUpdate
            ? CUSTOMER_ORDER_STATUS_IN_PROGRESS
            : CUSTOMER_ORDER_STATUS_DONE,
          unassignedBeacons: unassignedBeacons.currentUnassignedBeacons,
          webhook: true,
          subscription: {
            id: req.body[actionType].subscription.uuid,
            state: req.body[actionType].subscription.state,
            currency: req.body[actionType].subscription.currency,
            quantity: quantityBeacons,
            periodStart:
              req.body[actionType].subscription.current_period_started_at._,
            periodEnd:
              req.body[actionType].subscription.current_period_ends_at._,
            price: req.body[actionType].subscription.total_amount_in_cents._,
            subscription_add_ons: subscriptionAddons,
          },
        };

        const priceUpdated =
          parseInt(
            req.body[actionType].subscription.total_amount_in_cents._,
            10,
          ) + parseInt(subscriptionAddons.price || 0, 10);

        const date = moment().format('YYYY-MM-DD H:m:s');

        addHistoricalSubscription(
          req.body[actionType].account.email,
          date,
          priceUpdated,
        );

        const updatedUser = await registerUser(userInformation);

        const invoiceData = {
          collection_method: 'automatic',
        };

        const createdInvoices = await recApi.postInvoice(
          req.body[actionType].account.email,
          invoiceData,
        );

        generateResponse({
          responseInfo: {
            ...updatedUser,
            price: priceUpdated,
            createdInvoices,
          },
          res,
        });
        break;
      }
      case RECURLY_WEBHOOK_NEW_SUBSCRIPTION: {
        logger.debug(`${moment().format('YYYY-MM-DD H:m:s')} - ${actionType}`);
        console.log(`${moment().format('YYYY-MM-DD H:m:s')} - ${actionType}`);
        const newCustomer = await customersLocalAPI.getOne({
          email: req.body[actionType].account.email,
        });

        const reffererAccount = newCustomer.referredBy
          ? await customersLocalAPI.getOne({
              affiliateCode: newCustomer.referredBy,
            })
          : null;

        if (!newCustomer) {
          generateResponse({ responseInfo: {}, res });
          break;
        }

        const newSubscriptionAddons = {};
        if (
          req.body[actionType].subscription.subscription_add_ons
            .subscription_add_on
        ) {
          newSubscriptionAddons.code =
            req.body[
              actionType
            ].subscription.subscription_add_ons.subscription_add_on.add_on_code;
          newSubscriptionAddons.price =
            req.body[
              actionType
            ].subscription.subscription_add_ons.subscription_add_on.unit_amount_in_cents._;
          newSubscriptionAddons.quantity =
            req.body[
              actionType
            ].subscription.subscription_add_ons.subscription_add_on.quantity._;
        }

        const priceUpdated =
          parseInt(
            req.body[actionType].subscription.total_amount_in_cents._,
            10,
          ) + parseInt(newSubscriptionAddons.price || 0, 10);

        const date = moment().format('YYYY-MM-DD H:m:s');
        const referedByEmail = reffererAccount ? reffererAccount.email : null;

        await addNewUser(
          req.body[actionType].account.email,
          `${req.body[actionType].account.first_name} ${req.body[actionType]
            .account.last_name}`,
          priceUpdated,
          'Active',
          date,
          referedByEmail,
        );
        addHistoricalSubscription(
          req.body[actionType].account.email,
          date,
          priceUpdated,
        );
        generateResponse({ responseInfo: {}, res });

        break;
      }
      case RECURLY_WEBHOOK_EXPIRED_SUBSCRIPTION: {
        console.log(`${moment().format('YYYY-MM-DD H:m:s')} - ${actionType}`);
        logger.debug(`${moment().format('YYYY-MM-DD H:m:s')} - ${actionType}`);
        const userInformationExpired = {
          email: req.body[actionType].account.email,
          unassignedBeacons: 0,
          webhook: true,
          subscription: {},
        };

        const formData = {
          email: userInformationExpired.email,
          status: 'Inactive',
          apiKey: MARKETING_API_KEY,
        };

        updateUser(formData);

        const updatedUserExpired = await registerUser(userInformationExpired);

        generateResponse({ responseInfo: updatedUserExpired, res });
        break;
      }
      case RECURLY_WEBHOOK_CANCELED_SUBSCRIPTION: {
        console.log(`${moment().format('YYYY-MM-DD H:m:s')} - ${actionType}`);
        logger.debug(`${moment().format('YYYY-MM-DD H:m:s')} - ${actionType}`);
        const subscriptionAddonsCancelled = {};
        if (
          req.body[actionType].subscription.subscription_add_ons
            .subscription_add_on
        ) {
          subscriptionAddonsCancelled.code =
            req.body[
              actionType
            ].subscription.subscription_add_ons.subscription_add_on.add_on_code;
          subscriptionAddonsCancelled.price =
            req.body[
              actionType
            ].subscription.subscription_add_ons.subscription_add_on.unit_amount_in_cents._;
          subscriptionAddonsCancelled.quantity =
            req.body[
              actionType
            ].subscription.subscription_add_ons.subscription_add_on.quantity._;
        }
        const userInformationCancelled = {
          firstName: req.body[actionType].account.first_name,
          lastName: req.body[actionType].account.last_name,
          phoneNumber: req.body[actionType].account.phone,
          email: req.body[actionType].account.email,
          webhook: true,
          unassignedBeacons: 0,
          subscription: {
            id: req.body[actionType].subscription.uuid,
            state: req.body[actionType].subscription.state,
            currency: req.body[actionType].subscription.currency,
            quantity:
              parseInt(req.body[actionType].subscription.quantity._, 10) +
              parseInt(subscriptionAddonsCancelled.quantity || 0, 10),
            periodStart:
              req.body[actionType].subscription.current_period_started_at._,
            periodEnd:
              req.body[actionType].subscription.current_period_ends_at._,
            price: req.body[actionType].subscription.total_amount_in_cents._,
            subscription_add_ons: subscriptionAddonsCancelled,
          },
        };

        const formData = {
          email: userInformationCancelled.email,
          status: 'Inactive',
          apiKey: MARKETING_API_KEY,
        };

        updateUser(formData);

        const updatedUserCancelled = await registerUser(
          userInformationCancelled,
        );

        generateResponse({ responseInfo: updatedUserCancelled, res });

        break;
      }
      default: {
        generateResponse({ responseInfo: {}, res });
        break;
      }
    }
  });

  return app;
};
