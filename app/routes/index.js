// import mongoExpress from 'mongo-express/lib/middleware';
// import mongoExpressConfig from '../../config/mongo-config';

import express from 'express';

import customerRoutes from './customerRoutes';
import operatorRoutes from './operatorRoutes';
import gapiRoutes from './googleApiRoutes';
import mobileRoutes from './mobileRoutes';
import recurlyRoutes from './recurlyRoutes';
import webHookRoutes from './webHook';

import { OPERATOR_ROLE, ADMIN_ROLE, CUSTOMER_ROLE } from '../shared/constants';

// console.log(customerRoutes);

import {
  checkAccessRightsHandler,
  authMiddlewareHandler,
  authenticateUserHandler,
  registerUserHandler,
  logoutUserHandler,
  restorePasswordHandler,
  changePasswordHandler,
} from '../controllers';

const customerApp = express.Router();
const operatorApp = express.Router();
const googleApiApp = express.Router();
const mobileApp = express.Router();
const recurlyApp = express.Router();
const webHookApp = express.Router();

// const recurlyRoutes = express.Router();

export default app => {
  app.all('/*', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
    res.header(
      'Access-Control-Allow-Headers',
      'Content-type,Accept,X-Access-Token,X-Key,Authorization',
    );
    next();
  });

  app.options('/*', (req, res) => {
    res.status(200);
    res.end();
  });

  app.all('/api/v1/*', authMiddlewareHandler);

  // auth routes
  app.post('/api/v1/auth/register', registerUserHandler);
  app.post('/api/v1/auth/login', authenticateUserHandler);
  app.get(
    '/api/v1/auth/logout',
    (...rest) => {
      checkAccessRightsHandler(
        [ADMIN_ROLE, OPERATOR_ROLE, CUSTOMER_ROLE],
        ...rest,
      );
    },
    logoutUserHandler,
  );
  app.put('/api/v1/auth/forgot', restorePasswordHandler);
  app.put('/api/v1/auth/change', changePasswordHandler);

  app.use('/api/v1/customer', customerRoutes(customerApp));
  app.use('/api/v1/operator', operatorRoutes(operatorApp));
  app.use('/api/v1/gapi', gapiRoutes(googleApiApp));
  app.use('/api/v1/mobile', mobileRoutes(mobileApp));
  app.use('/api/v1/recurly', recurlyRoutes(recurlyApp));
  app.use('/api/v1/webhooks', webHookRoutes(webHookApp));

  // app.use('/mongo_express', mongoExpress(mongoExpressConfig));

  app.use((req, res) => {
    res.json({ error: 404 });
  });
};
