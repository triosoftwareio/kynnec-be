import { CUSTOMER_ROLE } from '../shared/constants';
import {
  checkAccessRightsHandler,
  getCustomerByToken,
  getCustomerBeaconsListHandler,
  getCustomerBeaconHandler,
  updateCustomerInfo,
  updateBeaconMessageRequestHandler,
  calculateCommissions,
  changeAffiliateCodeHandler,
} from '../controllers';

export default app => {
  app.all('*', (...rest) => {
    checkAccessRightsHandler([CUSTOMER_ROLE], ...rest);
  });

  app.get('/', getCustomerByToken);
  app.put('/:customerId', updateCustomerInfo);

  app.get('/beacons', getCustomerBeaconsListHandler);
  app.get('/beacons/:beaconId', getCustomerBeaconHandler);

  app.put(
    '/beacons/:id/attachment/:attachmentName',
    updateBeaconMessageRequestHandler,
  );

  app.post('/orders');
  app.get('/orders');
  app.get('/orders/:id');

  app.post('/affiliate/commission', calculateCommissions);

  app.post('/affiliate/changeCode', changeAffiliateCodeHandler);

  return app;
};
