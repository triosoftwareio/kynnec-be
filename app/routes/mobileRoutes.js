import {
  checkMobileAccessRightHandler,
  getCustomerByToken,
  getCustomerBeaconsListHandler,
  getCustomerBeaconHandler,
  decommissionBeaconsHandler,
  activateBeaconsHandler,
  deactivateBeaconsHandler,
  updateCustomerInfo,
  updateBeaconMessageRequestHandler,
} from '../controllers';

export default app => {
  app.all('*', checkMobileAccessRightHandler);

  app.get('/', getCustomerByToken);
  app.put('/:customerId', updateCustomerInfo);

  app.get('/beacons', getCustomerBeaconsListHandler);
  app.get('/beacons/:beaconId', getCustomerBeaconHandler);
  app.put('/beacons/:beaconId/decommission', decommissionBeaconsHandler);
  app.put('/beacons/:beaconId/activate', activateBeaconsHandler);
  app.put('/beacons/:beaconId/deactivate', deactivateBeaconsHandler);

  app.put(
    '/beacons/:id/attachment/:attachmentName',
    updateBeaconMessageRequestHandler,
  );

  return app;
};
