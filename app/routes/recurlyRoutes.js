import { createSubscription, lookUpAccount } from '../controllers';

export default app => {
  // app.all('*', (...rest) => {
  //   checkAccessRightsHandler([CUSTOMER_ROLE], ...rest);
  // });

  app.post('/createSubscription', createSubscription);
  app.get('/lookUpAccount', lookUpAccount);

  return app;
};
