import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import cookieParser from 'cookie-parser';
import xmlparser from 'express-xml-bodyparser';
import router from '../routes';

export default (app, express) => {
  app
    .use(express.static('./public'))
    .use(cookieParser())
    .use(
      xmlparser({
        explicitArray: false,
        normalize: false,
        normalizeTags: false,
        trim: true,
      }),
    )
    .use(bodyParser.json({ limit: '50mb' }))
    .use(
      bodyParser.urlencoded({
        extended: true,
      }),
    )
    .use(methodOverride());

  router(app);
};
