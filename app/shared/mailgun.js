import Mailgun from 'mailgun-js';
import config from '../../config/config.json';

/**
 * Function to send email with mailgun
 * @param {string} from - sender name(string)
 * @param {string} to - recipient name(string)
 * @param {string} html - html body of email
 * @param {string} subject - subject of email
 */

const sendMailgunEmail = (
  to,
  html,
  subject,
  attachment,
  callback = () => {},
) => {
  const mailgun = new Mailgun({
    apiKey: config.mailgun.api,
    domain: config.mailgun.domain,
  });

  const data = { to, subject, html, attachment };
  data.from = config.mailgun.fromName;

  mailgun.messages().send(data, callback);
};

export default sendMailgunEmail;
