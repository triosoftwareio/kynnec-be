export const ADMIN_ROLE = 'ADMIN_ROLE';
export const OPERATOR_ROLE = 'OPERATOR_ROLE';
export const CUSTOMER_ROLE = 'CUSTOMER_ROLE';

export const ACTIVE_STATUS = 'ACTIVE';
export const INACTIVE_STATUS = 'INACTIVE';

export const ORDER_NEW_STATUS = 'NEW';
export const ORDER_DEFAULT_STATUS = 'ORDERED';
export const ORDER_SHIPPED_STATUS = 'SHIPPED';
export const ORDER_PROVISIONED_STATUS = 'PROVISIONED';

export const CUSTOMER_ORDER_STATUS_IN_PROGRESS = 'IN PROGRESS';
export const CUSTOMER_ORDER_STATUS_DONE = 'DONE';

export const DEFAULT_PROJECT = 'brilliant-tide-178623';

export const MAILCHIMP_LIST_ID = '06fb4852e0';
export const MAILCHIMP_SUBSCRIBED_STATUS = 'subscribed';

/* eslint-disable */
export const PHONE_REGEX = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i;
export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const MOBILE_TOKEN =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6InRlc3RAZ21haWwuY29tIiwiZXhwaXJlIjozMTEwNDAwMCwiaWF0IjoxNTA3ODE3NjE2LCJleHAiOjE1Mzg5MjE2MTZ9.hfO3KVOyqybinIb4lYIhL_RxqvBHvIxOFxGYbHZlbcI';
