export const CURRENCY = 'USD';
export const REVENUE_SCHEDULE_TYPE = 'at_invoice';
export const ERROR_STATUS = 'error';
export const SUBSCRIPTION_TYPE = 'plan_royaltie_gem1';
export const SUBSCRIPTION_ADON_TYPE = 'addon_gem_1';
export const SUBSCRIPTION_CANCELED_STATUS = 'canceled';
export const REGULAR_DOMESTIC_SHIPMENT = 'Regular Domestic Shipment';
export const REGULAR_INTERNATIONAL_SHIPMENT = 'Regular International Shipment';
export const PRIORITY_DOMESTIC_SHIPMENT = 'Priority Domestic Shipment';
export const PRIORITY_INTERNATIONAL_SHIPMENT =
  'Priority International Shipment';
export const LASTMONTH_CHARGE_NAME = 'Last Month Subscription Charge';
export const CUSTOM_RECURLY_ERROR = 'Something gone wrong';
export const EXPIRED_STATUS = 'expired';

export const RECURLY_WEBHOOK_SUCCESSFUL_PAYMENT =
  'successful_payment_notification';
export const RECURLY_WEBHOOK_SUCCESSFUL_UPDATED =
  'updated_subscription_notification';
export const RECURLY_WEBHOOK_EXPIRED_SUBSCRIPTION =
  'expired_subscription_notification';
export const RECURLY_WEBHOOK_CANCELED_SUBSCRIPTION =
  'canceled_subscription_notification';
export const RECURLY_WEBHOOK_NEW_SUBSCRIPTION = 'new_subscription_notification';

export const RECURLY_ADDON_PRICE = 12;
export const RECURLY_CONVERT_IN_CENT = 100;
