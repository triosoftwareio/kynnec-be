import mongoose from 'mongoose';
import bluebird from 'bluebird';

import config from '../../config/config.json';

mongoose.Promise = bluebird;

const runDB = () => {
  mongoose.connect(config.mongoose.uri, { useMongoClient: true });

  mongoose.connection.on('error', err =>
    console.log('connection error:', err.message),
  );
  mongoose.connection.once('open', () => console.log('Connected to DB!'));
};

runDB();

export default {
  Schema: mongoose.Schema,
  mongoose,
};
