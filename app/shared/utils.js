// import rp from 'request-promise';
import requestRetry from 'requestretry';
import csv from 'fast-csv';
import fs from 'fs';
import errorHandler from '../controllers/errorHandler';
import { CUSTOM_ERROR, SOMETHING_GOES_WRONG } from '../shared/constants';
import Beacons from '../services/models/beaconsModel';
import customer from '../services/models/customersModel';
import usersModel from '../services/auth/userModel';

const { beacon } = Beacons;
const { customerModel } = customer;
const { Users } = usersModel;

const addUserUppBeacon = async (customerInfo, gemId) => {
  try {
    const isSystemUserExist = await Users.findOne({
      login: customerInfo.email,
    });

    const updateCustomerResponse = await customerModel.findOneAndUpdate(
      { email: customerInfo.email },
      customerInfo,
      {
        new: true,
        upsert: true,
      },
    );

    let savedSystemUser = {};
    if (!isSystemUserExist) {
      const systemUserOptions = {
        login: customerInfo.email,
        password: customerInfo.password,
      };
      systemUserOptions.customer = updateCustomerResponse._id;
      const newSystemUserEntity = new Users(systemUserOptions);
      savedSystemUser = await newSystemUserEntity.save();
    }

    const firstRegexp = new RegExp(`.*(R - ${gemId})$`, 'ig');
    const secondRegexp = new RegExp(
      `.*(@[a-z0-9]{1,20}\.[a-z]{2,5}[\s]-[\s]${gemId})`, //eslint-disable-line
      'ig',
    );
    const beaconUpdated = await beacon.multiUpdate(
      {
        $or: [{ description: firstRegexp }, { description: secondRegexp }],
      },
      { customer: updateCustomerResponse._id, stickerCode: gemId },
    );
    return {
      systemUser: !isSystemUserExist ? savedSystemUser : isSystemUserExist,
      beacon: beaconUpdated,
      customer: updateCustomerResponse,
    };
  } catch (error) {
    console.log(error);
    return error;
  }
};
const Helpers = {
  /**
   * Generating a response for each request
   * @param {int} status - response status code
   * @param {string} error - error object
   * @param {object} content - requested data when successed
   * @param {string} successMessage - success message text
   * @param {object} res - response object
   */

  generateCustomResponse: (
    status = 200,
    error = null,
    content,
    successMessage,
    res,
  ) => {
    const responseObject = {
      status,
      error,
      data: {
        content,
        message: successMessage || null,
      },
    };

    res.status(status);
    res.json(responseObject);
  },

  generateResponse: ({
    responseInfo,
    successMessage = '',
    errorMessage = '',
    res,
  }) => {
    try {
      const error = responseInfo.hasError ? responseInfo : null;
      const responseBody = responseInfo.hasError ? {} : responseInfo;

      const message = error ? error.message : errorMessage;

      const status = error ? responseInfo.statusCode : 200;

      const responseObject = {
        status,
        error,
        data: {
          content: responseBody,
          message: message || successMessage,
        },
      };

      res.status(status || 500);
      res.json(responseObject);
    } catch (error) {
      Helpers.generateResponse({
        responseInfo: errorHandler(SOMETHING_GOES_WRONG),
        res,
      });
    }
  },

  /**
   * Prepare object for datatables request
   * @param {object} req - request object
   * @returns {object} - object with datatable options and filters
   */

  prepareDtRequest: req => {
    const limit = req.query.limit || 10;
    const skip = req.query.skip || 0;
    const sort = {};

    if (req.query.sort_field) {
      let sortOrder = 1;

      if (req.query.sort_field[0] === '-') {
        sortOrder = -1;
        req.query.sort_field = req.query.sort_field.substr(1);
      }

      sort[req.query.sort_field] = sortOrder;
    } else {
      sort.time_created = -1;
    }
    return {
      limit,
      skip,
      sort,
    };
  },

  /**
   * Unique hash generator
   * @returns {object} - hashed string
   */

  guid: () => {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }

    return `${s4() + s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
  },

  serializeUserModel: user => ({
    login: user.login,
    token: user.tokens[user.tokens.length - 1],
  }),

  async sendRequest({
    url,
    method = 'GET',
    params = {},
    data = {},
    headers = {},
    auth = null,
    retryCallback = () => {},
  }) {
    let urlWithParams = url;
    const paramsCopy = Object.assign({}, params);

    Object.keys(paramsCopy).forEach(key => {
      const currentParam = paramsCopy[key];
      if (urlWithParams.includes(key)) delete paramsCopy[key];
      urlWithParams = urlWithParams.replace(`:${key}`, currentParam);
    });

    const options = {
      method,
      uri: urlWithParams,
      headers,
      json: true,
      useQuerystring: true,
      qs: paramsCopy,
      maxAttempts: 2,
      retryDelay: 100,
      retryStrategy: (err, response, body) => {
        if (err || body.error) {
          retryCallback(response);
          return true;
        }
      },
    };

    if (auth) options.auth = auth;

    if (Object.keys(data).length) options.body = data;

    try {
      const response = await requestRetry(options);

      if (response.statusCode > 300) {
        const customError = response.body.error
          ? response.body.error
          : {
              statusCode: 500,
              status: 500,
              message: 'Something goes wrong',
            };
        throw errorHandler(CUSTOM_ERROR, customError);
      }
      return response.body;
    } catch (error) {
      return error;
    }
  },

  parseCsvFile: async (fileName = `${__dirname}/../../dump.csv`) => {
    const stream = fs.createReadStream(fileName);
    csv
      .fromStream(stream, { ignoreEmpty: true, headers: true, delimiter: ',' })
      .transform((r, next) => {
        const row = r;
        delete row.lead_id;
        const userData = {
          email: row.email,
          firstName: row.first_name,
          lastName: row.last_name,
          address: row.address,
          city: row.city,
          state: row.state,
          zip: row.zip,
          country: row.country,
          affiliate_id: row['Affiliate ID'],
          affiliateCode: row['Affiliate ID'],
          referredBy: row['Referred by'],
          password: row.Password || 'Qwe12345',
        };
        const gemId = row.device_id;

        addUserUppBeacon(userData, gemId)
          .then(res => {
            next(null, res);
          })
          .catch(err => {
            next(err);
          });
      })
      .on('data', data => {
        console.log(data);
      })
      .on('end', () => {
        console.log('done');
        //  console.log(counter);
      });
  },
};

// Helpers.parseCsvFile();

export default Helpers;
