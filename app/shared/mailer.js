import fs from 'fs';
import ejs from 'ejs';
// import config from '../../config/config.json';
import sendMailgunEmail from './mailgun';

/**
 * Mailer class.
 * @constructor
 */

class Mailer {
  /**
     * Send email method
     * @param {object} options - object with config options
     * @param {function} callback - callback func after sending emails
     */

  sendEmail(options, callback = () => {}) {
    this._setOptions(options);

    this._getTemplate((err, content) => {
      if (err) {
        console.log(err);
        return;
      }

      const subject = this._getMailSubject();
      this.data.userEmail = this.emailTo;

      if (this.emailTo instanceof Array) {
        this.emailTo.forEach((email, index) => {
          this.data.userEmail = email;
          const renderedHtml = ejs.render(content, {
            data: this.data[index] ? this.data[index] : this.data,
          });
          sendMailgunEmail(
            this.emailTo[index],
            renderedHtml,
            subject,
            this.file,
            callback,
          );
        });
      } else {
        const renderedHtml = ejs.render(content, { data: this.data });
        sendMailgunEmail(
          this.emailTo,
          renderedHtml,
          subject,
          this.file,
          callback,
        );
      }
    });
  }

  /**
     * Get mail subject
     * @returns {string} - email subject string
     */

  _getMailSubject() {
    const subject = {
      thank_you: 'Thank you for payment',
      confirm_registration: 'Confirm your account',
      restore_password: 'Restore your password',
      password_changed: 'Password successfully changed',
    };
    if (this.subject) {
      return this.subject;
    }
    return subject[this.eventType];
  }

  /**
     * Get template html
     * @param {function} callback - callback function after getting template
     */

  _getTemplate(callback) {
    const filePath = `${__dirname}/../../email_templates/${this.eventType}.ejs`;
    fs.readFile(filePath, 'utf8', callback);
  }

  /**
     * Set options
     * @param {object} options - object with config options
     */

  _setOptions(options) {
    const defaultOptions = {
      eventType: 'thank_you',
      data: {},
      emailTo: 'v@codemotion.eu',
      file: null,
      subject: null,
    };

    Object.keys(defaultOptions).forEach(option => {
      this[option] =
        options && options[option] !== undefined
          ? options[option]
          : defaultOptions[option];
    });
  }
}

const mailer = new Mailer();

export default mailer;
