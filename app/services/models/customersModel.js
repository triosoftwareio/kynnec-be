import moment from 'moment';
import dataTables from 'mongoose-datatables';
import mongo from '../../shared/mongo';
import crudGenertor from './crudMixin';

import { CUSTOMER_ORDER_STATUS_IN_PROGRESS } from '../../shared/constants';

const { Schema, mongoose } = mongo;

const CustomerSchema = new Schema({
  mdc_id: {
    type: String,
  },
  email: {
    type: String,
    unique: true,
  },
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  phoneNumber: {
    type: String,
  },
  address: {
    type: String,
  },
  city: {
    type: String,
  },
  state: {
    type: String,
  },
  zip: {
    type: String,
  },
  country: {
    type: String,
  },
  contactType: {
    type: String,
  },
  dateCreated: {
    type: Date,
  },
  emailsOpens: {
    type: Number,
  },
  emailsClicks: {
    type: Number,
  },
  visits: {
    type: Number,
  },
  views: {
    type: Number,
  },
  tags: {
    type: String,
  },
  tagIds: {
    type: String,
  },
  gemOrderStatus: {
    type: String,
    default: CUSTOMER_ORDER_STATUS_IN_PROGRESS,
  },
  unassignedBeacons: {
    type: Number,
  },
  affiliate_id: {
    type: String,
  },
  referredBy: {
    type: String,
  },
  affiliateCode: {
    type: String,
  },
  color: {
    type: String,
  },
  subscription: {
    type: Object,
  },
  notes: {
    type: Object,
  },
  adjustmentValue: {
    type: Number,
  },
  internalMessage: {
    type: String,
  },
  externalMessage: {
    type: String,
  },
});

CustomerSchema.plugin(dataTables, {
  totalKey: 'recordsTotal',
  dataKey: 'data',
});

CustomerSchema.pre('save', async function(next) {
  const self = this;
  if (!self.isModified('dateCreated'))
    self.dateCreated = moment().unix() * 1000;
  next();
});

const Customer = mongoose.model('Customer', CustomerSchema);

export default { customer: crudGenertor(Customer), customerModel: Customer };
