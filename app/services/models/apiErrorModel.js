import moment from 'moment';
import dataTables from 'mongoose-datatables';
import mongo from '../../shared/mongo';
import crudGenertor from './crudMixin';

const { Schema, mongoose } = mongo;

const ApiErrorSchema = new Schema({
  projectId: {
    type: String,
  },
  errorCode: {
    type: String,
  },
  errorMessage: {
    type: String,
  },
  triesCounter: {
    type: Number,
    default: 0,
  },
  tryTime: {
    type: Date,
  },
  createdOn: {
    type: Date,
  },
});

ApiErrorSchema.plugin(dataTables, {
  totalKey: 'recordsTotal',
  dataKey: 'data',
});

ApiErrorSchema.pre('save', function(next) {
  const self = this;
  if (!self.isModified('createdOn')) self.createdOn = moment().unix() * 1000;

  next();
});

const ApiError = mongoose.model('ApiError', ApiErrorSchema);

export default { apiError: crudGenertor(ApiError), apiErrorModel: ApiError };
