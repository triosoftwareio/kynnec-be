import moment from 'moment';
import dataTables from 'mongoose-datatables';
import mongooseDeepPopulate from 'mongoose-deep-populate';
import mongo from '../../shared/mongo';
import crudGenertor from './crudMixin';
import { ACTIVE_STATUS } from '../../shared/constants';

const { Schema, mongoose } = mongo;

const deepPopulate = mongooseDeepPopulate(mongoose);

const ProjectSchema = new Schema({
  projectId: {
    type: String,
  },
  clientServiceEmail: {
    type: String,
  },
  status: {
    type: String,
    default: ACTIVE_STATUS,
  },
  accessToken: {
    token: String,
    createdOn: Date,
  },
  account: {
    type: Schema.Types.ObjectId,
    ref: 'Account',
  },
  purpose: {
    type: String,
  },
  isBlockedByGoogle: {
    type: Boolean,
    default: false,
  },
  createdOn: {
    type: Date,
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
  },
  updatedOn: {
    type: Date,
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
  },
});

ProjectSchema.plugin(dataTables, {
  totalKey: 'recordsTotal',
  dataKey: 'data',
});

ProjectSchema.plugin(deepPopulate, {});

ProjectSchema.pre('save', function(next) {
  const self = this;
  if (!self.isModified('createdOn')) self.createdOn = moment().unix() * 1000;

  next();
});

const Project = mongoose.model('Project', ProjectSchema);

export default { project: crudGenertor(Project), projectModel: Project };
