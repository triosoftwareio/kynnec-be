import moment from 'moment';
import dataTables from 'mongoose-datatables';
import mongo from '../../shared/mongo';
import crudGenertor from './crudMixin';
import { ORDER_DEFAULT_STATUS } from '../../shared/constants';

const { Schema, mongoose } = mongo;

const OrderSchema = new Schema({
  status: {
    type: String,
    default: ORDER_DEFAULT_STATUS,
  },
  totalPrice: {
    type: String,
  },
  monthlySubscriptionPrice: {
    type: String,
  },
  numberOfBeacons: {
    type: Number,
  },
  beacons: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Beacon',
    },
  ],
  createdOn: {
    type: Date,
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
  },
  updatedOn: {
    type: Date,
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
  },
});

OrderSchema.plugin(dataTables, {
  totalKey: 'recordsTotal',
  dataKey: 'data',
});

OrderSchema.pre('save', function(next) {
  const self = this;
  if (!self.isModified('createdOn')) self.createdOn = moment().unix() * 1000;

  next();
});

const Order = mongoose.model('Order', OrderSchema);

export default { order: crudGenertor(Order), orderModel: Order };
