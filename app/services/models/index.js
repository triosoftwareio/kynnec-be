import moment from 'moment';
import btoa from 'btoa';
import atob from 'atob';
import utils from '../../shared/utils';
import { getAccessTokenHandler } from '../api/localApi';
import errorHandler from '../../controllers/errorHandler';
import {
  MISSED_ACCESS_TOKEN,
  CUSTOM_ERROR,
  PAGE_SIZE,
} from '../../shared/constants';

const PROXIMITYBEACON_API_URL =
  'https://proximitybeacon.googleapis.com/v1beta1';

const GAPIqueryWrapper = async ({
  url,
  method = 'GET',
  body = {},
  params = {},
  retryCallback,
}) => {
  try {
    const requestUrl = `${PROXIMITYBEACON_API_URL}/${url}`;
    const response = await getAccessTokenHandler(params.projectId);

    if (response.hasError) throw errorHandler(MISSED_ACCESS_TOKEN);

    const headers = {
      Authorization: `Bearer ${response}`,
    };

    return utils.sendRequest({
      url: requestUrl,
      params,
      headers,
      method,
      data: body,
      retryCallback: retryResponse => {
        const retryOptions = {
          projectId: params.projectId,
          errorCode: retryResponse.statusCode,
          errorMessage: retryResponse.statusMessage,
          triesCounter: retryResponse.attempts,
          tryTime: moment().unix(),
        };

        retryCallback(retryOptions);
      },
    });
  } catch (err) {
    return err;
  }
};

export const beaconAPI = {
  list: async options => {
    try {
      /* eslint-disable */
      const result = [];
      let response = null;
      options.params.pageSize = PAGE_SIZE;
      do {
        response = await GAPIqueryWrapper({ url: 'beacons', ...options });
        options.params.pageToken = response.nextPageToken;
        result.push(...response.beacons);
      } while (
        response &&
        response.beacons &&
        response.beacons.length === PAGE_SIZE
      );
      return result;
      /* eslint-enable */
    } catch (error) {
      return errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: error,
      });
    }
  },
  fetch: options => GAPIqueryWrapper({ url: 'beacons/:name', ...options }),
  remove: options =>
    GAPIqueryWrapper({ url: 'beacons/:name', method: 'DELETE', ...options }),
  decommission: options =>
    GAPIqueryWrapper({
      url: `beacons/:name:decommission`,
      method: 'POST',
      ...options,
    }),
  activate: options =>
    GAPIqueryWrapper({
      url: 'beacons/:name:activate',
      method: 'POST',
      ...options,
    }),
  deactivate: options =>
    GAPIqueryWrapper({
      url: 'beacons/:name:deactivate',
      method: 'POST',
      ...options,
    }),
  update: options =>
    GAPIqueryWrapper({ url: 'beacons/:name', method: 'PUT', ...options }),
  register: options =>
    GAPIqueryWrapper({ url: 'beacons:register', method: 'POST', ...options }),
};

export const attachmentAPI = {
  list: async options => {
    const attachmentsList = await GAPIqueryWrapper({
      url: `beacons/:name/attachments`,
      ...options,
    });

    if (!attachmentsList.attachments || !attachmentsList.attachments.length)
      return [];

    return attachmentsList.attachments.map(el => {
      const data = el;
      const tempData = JSON.parse(atob(data.data));
      data.notificationMessage = {
        title: tempData.title,
        url: tempData.url,
        targeting: tempData.targeting || {},
      };
      return data;
    });
  },
  create: async options => {
    const optionsToPrepare = Object.assign({}, options);
    const { namespacedType, title, url, targeting } = optionsToPrepare.body;

    optionsToPrepare.body = {
      namespacedType: namespacedType || 'com.google.nearby/en',
      data: btoa(JSON.stringify({ title, url, targeting })),
    };

    const createdAttachmentResponse = await GAPIqueryWrapper({
      url: 'beacons/:name/attachments',
      method: 'POST',
      ...optionsToPrepare,
    });

    const atobString = JSON.parse(atob(createdAttachmentResponse.data));
    const notificationMessage = {
      title: atobString.title,
      url: atobString.url,
      targeting: atobString.targeting || {},
    };

    return {
      ...createdAttachmentResponse,
      notificationMessage,
    };
  },
  delete: options =>
    GAPIqueryWrapper({
      url: 'beacons/:name/attachments/:attachmentName',
      method: 'DELETE',
      ...options,
    }),
};
