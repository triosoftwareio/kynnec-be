import mongo from '../../shared/mongo';
import errorHandler from '../../controllers/errorHandler';
import { SOMETHING_GOES_WRONG } from '../../shared/constants';

const { mongoose } = mongo;

export default Schema => ({
  create: options => {
    const newSchemaObject = new Schema(options);
    return newSchemaObject.save();
  },
  getById: id => {
    if (!mongoose.Types.ObjectId.isValid(id))
      return errorHandler(SOMETHING_GOES_WRONG);
    return Schema.findOne({ _id: id });
  },
  getOne: options => {
    if (options._id && !mongoose.Types.ObjectId.isValid(options._id))
      return errorHandler(SOMETHING_GOES_WRONG);
    return Schema.findOne(options);
  },
  list: options => Schema.find(options),
  update: (findOptions, updateOptions) =>
    Schema.findOneAndUpdate(findOptions, updateOptions, { new: true }),
  updateOrCreate: (findOptions, updateOptions) =>
    Schema.findOneAndUpdate(findOptions, updateOptions, {
      new: true,
      upsert: true,
    }),
  remove: options => Schema.findOneAndRemove(options),
  dataTable: options => Schema.dataTables(options),
  multiUpdate: (findOptions, updateOptions) =>
    Schema.update(findOptions, updateOptions, { multi: true }),
});
