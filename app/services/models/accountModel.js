import moment from 'moment';
import dataTables from 'mongoose-datatables';
import mongo from '../../shared/mongo';
import crudGenertor from './crudMixin';

const { Schema, mongoose } = mongo;

const AccountSchema = new Schema({
  developerAccount: {
    type: String,
    required: true,
    unique: true,
  },
  publicKey: {
    type: String,
    unique: true,
  },
  privateKey: {
    type: String,
    unique: true,
  },
  filePath: {
    type: String,
  },
  createdOn: {
    type: Date,
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
  },
  updatedOn: {
    type: Date,
  },
  updatedBy: {
    type: mongoose.Schema.Types.ObjectId,
  },
});

AccountSchema.plugin(dataTables, {
  totalKey: 'recordsTotal',
  dataKey: 'data',
});

AccountSchema.pre('save', function(next) {
  const self = this;
  if (!self.isModified('createdOn')) self.createdOn = moment().unix() * 1000;

  next();
});

AccountSchema.pre('update', function(doc, next) {
  this.update({}, { $set: { updatedOn: moment().unix() * 1000 } });
  next();
});

const Account = mongoose.model('Account', AccountSchema);

export default { account: crudGenertor(Account), accountModel: Account };
