import moment from 'moment';
import dataTables from 'mongoose-datatables';
import mongooseDeepPopulate from 'mongoose-deep-populate';
import mongo from '../../shared/mongo';
import crudGenertor from './crudMixin';

import { ORDER_NEW_STATUS } from '../../shared/constants';

const { Schema, mongoose } = mongo;

const deepPopulate = mongooseDeepPopulate(mongoose);

const BeaconSchema = new Schema({
  beaconName: {
    type: String,
    unique: true,
  },
  category: {
    type: String,
  },
  customer: {
    type: Schema.Types.ObjectId,
    ref: 'Customer',
    default: null,
  },
  project: {
    type: Schema.Types.ObjectId,
    ref: 'Project',
  },
  developerAccount: {
    type: String,
  },
  projectId: {
    type: String,
  },
  description: {
    type: String,
  },
  status: {
    type: String,
  },
  orderStatus: {
    type: String,
    default: ORDER_NEW_STATUS,
  },
  advertisedId: {
    type: Object,
  },
  attachments: {
    type: Array,
  },
  attachmentsRequest: {
    type: Array,
    default: [],
  },
  eTag: {
    type: String,
  },
  expectedStability: {
    type: String,
  },
  indoorLevel: {
    type: Object,
  },
  latLng: {
    type: Object,
  },
  placeId: {
    type: String,
  },
  properties: {
    type: Object,
  },
  provisioningKey: {
    type: String,
  },
  createdOn: {
    type: Date,
  },
  createdBy: {
    type: String,
  },
  updatedOn: {
    type: Date,
  },
  updatedBy: {
    type: String,
  },
  stickerCode: {
    type: String,
  },
});

BeaconSchema.plugin(dataTables, {
  totalKey: 'recordsTotal',
  dataKey: 'data',
});

BeaconSchema.plugin(deepPopulate, {});

BeaconSchema.pre('save', function(next) {
  const self = this;
  if (!self.isModified('createdOn')) self.createdOn = moment().unix() * 1000;

  next();
});

BeaconSchema.pre('update', function() {
  this.update({}, { $set: { updatedOn: moment().unix() * 1000 } });
});
BeaconSchema.pre('findOneAndUpdate', function() {
  this.update({}, { $set: { updatedOn: moment().unix() * 1000 } });
});

const Beacon = mongoose.model('Beacon', BeaconSchema);

export default { beacon: crudGenertor(Beacon), beaconModel: Beacon };
