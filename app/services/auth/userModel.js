import bcrypt from 'bcryptjs';
import moment from 'moment';
import dataTables from 'mongoose-datatables';
import mongooseDeepPopulate from 'mongoose-deep-populate';
import mongo from '../../shared/mongo';

import { ACTIVE_STATUS, CUSTOMER_ROLE } from '../../shared/constants';

const Schema = mongo.Schema;
const mongoose = mongo.mongoose;

const deepPopulate = mongooseDeepPopulate(mongoose);

const Users = new Schema({
  login: {
    type: String,
    unique: true,
  },
  tokens: {
    type: Array,
    default: [],
  },
  password: {
    type: String,
  },
  status: {
    type: String,
    default: ACTIVE_STATUS,
  },
  restoreHash: {
    type: String,
  },
  restoreHashExpire: {
    type: Date,
  },
  roles: {
    type: Array,
    default: [CUSTOMER_ROLE],
  },
  customer: {
    type: Schema.Types.ObjectId,
    ref: 'Customer',
  },
  impersonateLogins: {
    type: Array,
    default: [],
  },
});

Users.plugin(dataTables, {
  totalKey: 'recordsTotal',
  dataKey: 'data',
});

Users.plugin(deepPopulate, {});

Users.pre('save', async function(next) {
  const SALT_FACTOR = 6;
  const self = this;

  const salt = await bcrypt.genSalt(SALT_FACTOR);
  const hashedPass = await bcrypt.hash(self.password, salt);

  if (!self.isModified('time_created'))
    self.time_created = moment().unix() * 1000;
  if (self.isModified('password')) self.password = hashedPass;

  next();
});

Users.methods.comparePassword = function(password) {
  return bcrypt.compare(password, this.password);
};

const UsersObject = mongoose.model('Users', Users);

export default { Users: UsersObject };
