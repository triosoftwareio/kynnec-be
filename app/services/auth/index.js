import jwt from 'jsonwebtoken';
import crypto from 'mz/crypto';
import moment from 'moment';
import usersModel from './userModel';
import Mailer from '../../shared/mailer';
import config from '../../../config/config.json';
import errorHandler from '../../controllers/errorHandler';
import Helpers from '../../shared/utils';
import { customersLocalAPI } from '../api/localApi';
import { addNewListMember } from '../api/mailchimpApi';
import {
  SOMETHING_GOES_WRONG,
  CUSTOM_ERROR,
  ACCESS_RESTRICTED,
  CUSTOMER_ROLE,
  OPERATOR_ROLE,
  WRONG_CREDENTIALS,
  ACTIVE_STATUS,
  MOBILE_TOKEN,
} from '../../shared/constants';

const { generateCustomResponse } = Helpers;
const { Users } = usersModel;

const unauthorizedRoutes = [
  'auth/login',
  'auth/register',
  'auth/forgot',
  'auth/change',
  'recurly/createSubscription',
  'webhooks',
];

const checkIfUnauthorizedRoute = requestedMethod =>
  unauthorizedRoutes.some(e => requestedMethod.includes(e));

export const authMiddleware = (req, res, next) => {
  const requestedMethod = req.params[0];

  const authHeader = req.headers.authorization;
  const token = authHeader ? authHeader.split('Bearer ')[1] : null;

  if (token && checkIfUnauthorizedRoute(requestedMethod)) {
    generateCustomResponse(
      403,
      errorHandler(ACCESS_RESTRICTED),
      null,
      null,
      res,
    );
    return;
  }

  if (!token && !checkIfUnauthorizedRoute(requestedMethod)) {
    generateCustomResponse(
      403,
      errorHandler(ACCESS_RESTRICTED),
      null,
      null,
      res,
    );
    return;
  }
  next();
};

export const checkAccessRights = async (roles, req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader ? authHeader.split('Bearer ')[1] : null;

  try {
    let user = null;
    if (roles.includes(CUSTOMER_ROLE))
      user = await Users.findOne({ tokens: token }).populate('customer');
    else user = await Users.findOne({ tokens: token });

    if (!user || !roles.some(r => user.roles.includes(r))) {
      generateCustomResponse(
        403,
        errorHandler(ACCESS_RESTRICTED),
        null,
        null,
        res,
      );
      return;
    }

    user.token = token;
    req.user = user;

    next();
  } catch (error) {
    generateCustomResponse(
      500,
      errorHandler(SOMETHING_GOES_WRONG),
      null,
      null,
      res,
    );
  }
};

export const checkMobileAccess = async (req, res, next) => {
  try {
    const authHeader = req.headers.authorization;
    const token = authHeader ? authHeader.split('Bearer ')[1] : null;
    const { username } = req.query;

    if (token !== MOBILE_TOKEN || !username) {
      generateCustomResponse(
        403,
        errorHandler(ACCESS_RESTRICTED),
        null,
        null,
        res,
      );
      return;
    }

    const customerResponse = await customersLocalAPI.getOne({
      email: username,
    });

    if (!customerResponse) {
      generateCustomResponse(
        403,
        errorHandler(ACCESS_RESTRICTED),
        null,
        null,
        res,
      );
      return;
    }

    req.user = customerResponse;
    req.isMobileAction = true;
    next();
  } catch (error) {
    generateCustomResponse(
      500,
      errorHandler(SOMETHING_GOES_WRONG),
      null,
      null,
      res,
    );
  }
};

const generateJWT = user => {
  const tokenExpire = 12 * 2592000; // 12 month
  const signedUser = {
    login: user.login,
    expire: tokenExpire,
  };

  return jwt.sign(signedUser, config.jwt.secret, {
    expiresIn: tokenExpire,
  });
};

export const prepareFEUserModel = userModel => ({
  login: userModel.login,
  customer: userModel.customer,
  roles: userModel.roles,
  token: userModel.token || userModel.tokens[userModel.tokens.length - 1],
});

export const updateCustomer = async (id, customerInfo) =>
  customersLocalAPI.update({ _id: id }, { ...customerInfo });

export const registerUser = async customerInfo => {
  try {
    const isUserExist = await Users.findOne({ login: customerInfo.email });
    if (isUserExist) {
      const updateInfo = customerInfo;

      const oldCustomerData = await customersLocalAPI.getOne({
        _id: isUserExist.customer,
      });

      if (!updateInfo.webhook) {
        delete updateInfo.unassignedBeacons;
      }

      if (updateInfo.afiliate && !oldCustomerData.affiliateCode) {
        const cryptoBytes = await crypto.randomBytes(8);
        updateInfo.affiliateCode = cryptoBytes.toString('hex');
      }

      const updatedCustomer = await updateCustomer(
        isUserExist.customer,
        updateInfo,
      );
      const userUpdate = await Users.findOne({
        customer: updatedCustomer._id,
      }).populate('customer');

      return { savedSystemUser: userUpdate, newCustomer: updatedCustomer };
    }

    const {
      firstName,
      lastName,
      phoneNumber,
      email,
      country,
      affiliate_id,
      subscription,
      unassignedBeacons,
      referredBy,
      address,
      zip,
      state,
      city,
    } = customerInfo;
    const systemUserOptions = { login: email, password: customerInfo.password };

    let affiliateCode = null;
    if (customerInfo.afiliate) {
      const cryptoBytes = await crypto.randomBytes(8);
      affiliateCode = cryptoBytes.toString('hex');
    }

    const newCustomer = await customersLocalAPI.create({
      firstName,
      lastName,
      phoneNumber,
      email,
      country,
      affiliate_id,
      subscription,
      unassignedBeacons,
      referredBy,
      address,
      zip,
      state,
      city,
      affiliateCode,
    });

    systemUserOptions.customer = newCustomer._id;

    const newSystemUserEntity = new Users(systemUserOptions);
    const savedSystemUser = await newSystemUserEntity.save();

    addNewListMember(email, firstName, lastName);

    return { newCustomer, savedSystemUser };
  } catch (error) {
    console.log(error);
    return error;
  }
};

export const createAdminUser = async () => {
  try {
    const login = 'oper@gmail.com';
    const isUserExist = await Users.findOne({ login });

    if (isUserExist) return null;

    const userEntity = new Users({
      login,
      password: '123456',
      roles: [OPERATOR_ROLE],
      status: ACTIVE_STATUS,
    });

    const savedUser = await userEntity.save();

    return savedUser;
  } catch (error) {
    return error;
  }
};

export const impersonate = async (customerId, systemUser) => {
  try {
    const userToImpersonate = await Users.findOne({
      customer: customerId,
      roles: CUSTOMER_ROLE,
    });

    if (!userToImpersonate) throw errorHandler(ACCESS_RESTRICTED);

    const jwtToken = generateJWT(userToImpersonate);
    userToImpersonate.tokens.push(jwtToken);

    const impersonateLoginObject = {
      userId: systemUser._id,
      login: systemUser.login,
      loginAt: moment().unix(),
    };

    if (userToImpersonate.impersonateLogins)
      userToImpersonate.impersonateLogins = [];
    userToImpersonate.impersonateLogins.push(impersonateLoginObject);

    const savedUser = await userToImpersonate.save();
    const populatedUser = await savedUser.deepPopulate('customer');
    return prepareFEUserModel(populatedUser);
  } catch (error) {
    return error;
  }
};

// createAdminUser();

export const authenticate = async (login, password) => {
  try {
    const user = await Users.findOne({ login, status: ACTIVE_STATUS });
    if (!user) throw errorHandler(WRONG_CREDENTIALS);

    const isPasswordsMatch = await user.comparePassword(password);
    if (!isPasswordsMatch) throw errorHandler(WRONG_CREDENTIALS);

    const jwtToken = generateJWT(user);
    user.tokens.push(jwtToken);

    const savedUser = await user.save();
    const populatedUser = await savedUser.deepPopulate('customer');
    return prepareFEUserModel(populatedUser);
  } catch (error) {
    console.log(error);
    return error;
  }
};

export const logout = async token => {
  try {
    const user = await Users.findOne({ tokens: token });
    if (!user) throw errorHandler(ACCESS_RESTRICTED);

    const tokenIndex = user.tokens.indexOf(token);
    user.tokens.splice(tokenIndex, 1);
    user.markModified('tokens');
    await user.save();

    return {};
  } catch (error) {
    console.log(error);
    return error;
  }
};

export const generateRestorePasswordLink = async login => {
  try {
    const expireTime = 3600; // One hour in seconds
    const user = await Users.findOne({ login });
    if (!user) throw errorHandler(WRONG_CREDENTIALS);

    const cryptoBytes = await crypto.randomBytes(15);
    const link = cryptoBytes.toString('hex');
    const linkExpire = moment().unix() + expireTime;

    user.restoreHash = link;
    user.restoreHashExpire = linkExpire;
    await user.save();

    Mailer.sendEmail({
      eventType: 'restore_password',
      data: {
        userName: user.login,
        url: `${config.appDomain}/auth/restore-password/${user.restoreHash}`,
      },
      emailTo: user.login,
    });

    return { restoreHash: link };
  } catch (error) {
    return error;
  }
};

export const changeUserPassword = async (restoreHash, password) => {
  try {
    const user = await Users.findOne({ restoreHash });

    if (!user)
      throw errorHandler(CUSTOM_ERROR, {
        code: 422,
        message: 'Wrong restore token',
      });

    const nowDate = moment().unix();

    if (nowDate > user.restoreHashExpire)
      throw errorHandler(CUSTOM_ERROR, {
        code: 422,
        message: 'Link has expired',
      });

    user.restoreHash = null;
    user.restoreHashExpire = null;

    user.password = password;

    const savedUser = await user.save();
    const populatedUser = await savedUser.deepPopulate('customer');

    Mailer.sendEmail({
      eventType: 'password_changed',
      data: {
        userName: user.login,
        url: `${config.appDomain}/`,
      },
      emailTo: user.login,
    });

    return prepareFEUserModel(populatedUser);
  } catch (error) {
    return error;
  }
};
/* eslint-enable */
