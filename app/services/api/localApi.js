import moment from 'moment';
import Account from '../models/accountModel';
import Beacon from '../models/beaconsModel';
import Project from '../models/projectModel';
import Customer from '../models/customersModel';
import Order from '../models/orderModel';
import ApiError from '../models/apiErrorModel';
import errorHandler from './../../controllers/errorHandler';
import { beaconUpdate } from './beaconUpdateApi';
import {
  fetchAccessToken,
  makeRequest,
  addDefaultAttachment,
  updateAttachment,
} from './googleApi';

import {
  CREATE_ATTACHMENT,
  DELETE_ATTACHMENT,
  PHONE_REGEX,
  EMAIL_REGEX,
  SOMETHING_GOES_WRONG,
  ACTIVATE_BEACON,
  DEACTIVATE_BEACON,
  ORDER_DEFAULT_STATUS,
  ORDER_PROVISIONED_STATUS,
  DEFAULT_PROJECT,
  FETCH_BEACONS_LIST,
  CUSTOM_ERROR,
  MISSED_PARAMS,
  CUSTOMER_ORDER_STATUS_IN_PROGRESS,
  CUSTOMER_ORDER_STATUS_DONE,
} from '../../shared/constants';

import Helpers from '../../shared/utils';

const { account } = Account;
const { order } = Order;
const { beacon, beaconModel } = Beacon;
const { project, projectModel } = Project;
const { customer } = Customer;
const { prepareDtRequest } = Helpers;
const { apiError } = ApiError;

const validateAttachment = (attachment, customerObject) => {
  const attachmentObject = Object.assign({}, attachment);
  let { title } = attachmentObject;
  let hasError = false;

  if (
    PHONE_REGEX.test(title) ||
    EMAIL_REGEX.test(title) ||
    title.includes(customerObject.firstName) ||
    title.includes(customerObject.lastName)
  ) {
    hasError = true;
  }

  title = title
    .replace(PHONE_REGEX, '')
    .replace(EMAIL_REGEX, '')
    .replace(customerObject.firstName, '')
    .replace(customerObject.lastName, '')
    .replace();

  attachmentObject.title = title;

  return { attachmentObject, hasError };
};

export const removeAttachmentRestrictStrings = async (
  attachment,
  customerObject,
  beaconName,
  userEmail,
) => {
  const { attachmentObject, hasError } = validateAttachment(
    attachment,
    customerObject,
  );
  if (hasError) {
    const promises = [
      makeRequest(DELETE_ATTACHMENT, attachmentObject),
      makeRequest(CREATE_ATTACHMENT, { beaconName, ...attachment, userEmail }),
    ];
    try {
      const googleApiResult = await Promise.all(promises);
      const localResult = await beacon.update(
        { beaconName },
        { attachments: [googleApiResult[1]] },
      );
      console.log(localResult);
    } catch (e) {
      console.log(e);
      return e;
    }
  }
};

export const accountsLocalAPI = {
  ...account,
  accountsListDataTable: async (req, options) => {
    const dtParams = prepareDtRequest(req);
    dtParams.search = req.query.keyword
      ? {
          value: req.query.keyword,
          fields: ['title'],
        }
      : {};

    try {
      const dtList = await account.dataTable(
        Object.assign({}, dtParams, options),
      );
      return dtList;
    } catch (e) {
      console.log(e);
      return e;
    }
  },
};

export const projectLocalAPI = {
  ...project,
  projectsListDataTable: async (req, options) => {
    const dtParams = prepareDtRequest(req);
    dtParams.search = req.query.keyword
      ? {
          value: req.query.keyword,
          fields: ['projectId, clientServiceEmail'],
        }
      : {};

    try {
      const dtResponse = await projectModel.dataTables(
        Object.assign({}, dtParams, options),
      );
      const populatedResponse = await projectModel.deepPopulate(
        dtResponse.data,
        'account',
      );

      // TODO: optimize this shit
      const resultReponse = Promise.all(
        populatedResponse.map(async pr => {
          const projectObj = pr.toObject();
          const projectBeacons = await beacon.list({ project: pr._id });
          projectObj.numberOfBeacons = projectBeacons
            ? projectBeacons.length
            : 0;
          return projectObj;
        }),
      );

      return {
        data: await resultReponse,
        recordsTotal: dtResponse.recordsTotal,
      };
    } catch (e) {
      console.log(e);
      return e;
    }
  },
  getProjectWithBeacons: options =>
    projectModel
      .findOne(options)
      .populate('beacons')
      .exec(),
};

export const orderLocalAPI = { ...order };

export const beaconsLocalAPI = {
  ...beacon,
  getWithPopulate: async options => {
    try {
      const currentBeacon = await beaconsLocalAPI.getOne(options);
      if (currentBeacon && currentBeacon.hasError)
        throw errorHandler(SOMETHING_GOES_WRONG);

      return (
        currentBeacon &&
        currentBeacon.deepPopulate('project project.account customer')
      );
    } catch (error) {
      return error;
    }
  },
  changeBeaconStatus: async (status, options) => {
    try {
      const methodName =
        status === 'active' ? ACTIVATE_BEACON : DEACTIVATE_BEACON;

      const gapiResponse = await makeRequest(methodName, { params: options });

      if (gapiResponse.hasError) throw errorHandler(SOMETHING_GOES_WRONG);

      return beaconsLocalAPI.update(
        { beaconName: `beacons/${options.name}` },
        { status },
      );
    } catch (error) {
      return error;
    }
  },
  updateProjectId: (beaconsIds, projectId) =>
    beaconModel.update(
      {
        _id: {
          $in: beaconsIds,
        },
      },
      {
        projectId,
      },
    ),
  beaconsListDataTable: async (req, options) => {
    const dtParams = prepareDtRequest(req);
    dtParams.search = req.query.keyword
      ? {
          value: req.query.keyword,
          fields: [
            'accountEmail',
            'description',
            //      'projectId',
            'orderStatus',
            'status',
          ],
        }
      : {};

    const dtResponse = await beaconModel.dataTables(
      Object.assign({}, dtParams, options),
    );

    if (!dtResponse.data.length) {
      /* eslint-disable */
      const customerSearch = await customersLocalAPI.getOne({
        $or: [
          { firstName: { $regex: req.query.keyword, $options: 'i' } },
          { lastName: { $regex: req.query.keyword, $options: 'i' } },
          { email: { $regex: req.query.keyword, $options: 'i' } },
        ],
      });
      /* eslint-enable */

      if (customerSearch) {
        delete dtParams.search;
        dtParams.find = {
          customer: customerSearch._id,
        };
      }
      const customerResponse = await beaconModel.dataTables(
        Object.assign({}, dtParams, options),
      );
      dtResponse.data = customerResponse.data;
      dtResponse.recordsTotal = customerResponse.recordsTotal;
    }

    const populatedResponse = await beaconModel.deepPopulate(
      dtResponse.data,
      'project project.account customer',
    );

    return {
      data: populatedResponse,
      recordsTotal: dtResponse.recordsTotal,
    };
  },
  updateBeaconAttachmentRequest: async (
    beaconId,
    customerId,
    newAttachment,
  ) => {
    const currentBeacon = await beaconModel
      .findOne({
        _id: beaconId,
        customer: customerId,
      })
      .populate('customer');

    const { attachmentObject } = validateAttachment(
      newAttachment.notificationMessage,
      currentBeacon.customer,
    );

    currentBeacon.attachmentsRequest = [
      { ...newAttachment, notificationMessage: attachmentObject },
    ];
    currentBeacon.markModified('attachmentsRequest');

    return currentBeacon.save();
  },
  declineBeaconAttachmentRequest: async beaconId => {
    const currentBeacon = await beacon.getById(beaconId).populate('project');
    const currentAttachment = currentBeacon.attachmentsRequest[0];
    const previousAttachment = currentBeacon.attachments[0];

    const updateResult = await updateAttachment(
      currentBeacon.project.projectId,
      currentBeacon.beaconName,
      currentAttachment.attachmentName,
      previousAttachment.notificationMessage,
    );

    return beacon.update(
      { _id: beaconId },
      { attachmentsRequest: [], attachments: [updateResult] },
    );
  },
  approveBeaconAttachmentRequest: async beaconId => {
    const currentBeacon = await beaconModel.findOne({ _id: beaconId });

    currentBeacon.attachments = currentBeacon.attachmentsRequest;
    currentBeacon.attachmentsRequest = [];
    currentBeacon.markModified('attachments');
    currentBeacon.markModified('attachmentsRequest');

    return currentBeacon.save();
  },
  addBeaconAttachment: async (beaconId, attachment) => {
    const currentBeacon = await beaconModel
      .findOne({ _id: beaconId })
      .populate('customer');

    const { attachmentObject } = validateAttachment(
      attachment,
      currentBeacon.owner,
    );
    return beacon.update(
      { _id: beaconId },
      { attachments: [attachmentObject] },
    );
  },
  provisionBeacon: async (beaconId, customerId) => {
    try {
      const localBeacon = await beaconsLocalAPI.getById(beaconId);
      const currentCustomer = await customer.getOne({ _id: customerId });

      if (!localBeacon || !currentCustomer) throw errorHandler(MISSED_PARAMS);

      const params = {
        name: localBeacon.beaconName.split('/')[1],
        projectId: DEFAULT_PROJECT,
      };
      const changeBeaconStatusResponse = await beaconsLocalAPI.changeBeaconStatus(
        'active',
        params,
      );

      if (changeBeaconStatusResponse.hasError)
        return errorHandler(changeBeaconStatusResponse);

      localBeacon.customer = currentCustomer._id;
      localBeacon.orderStatus = ORDER_PROVISIONED_STATUS;

      currentCustomer.unassignedBeacons =
        currentCustomer.unassignedBeacons > 0
          ? parseInt(currentCustomer.unassignedBeacons - 1, 10)
          : 0;

      currentCustomer.gemOrderStatus =
        currentCustomer.unassignedBeacons === 0
          ? CUSTOMER_ORDER_STATUS_DONE
          : CUSTOMER_ORDER_STATUS_IN_PROGRESS;

      await currentCustomer.save();

      return localBeacon.save();
    } catch (error) {
      return error;
    }
  },
  removeBeaconAttachment: beaconId =>
    beacon.update({ _id: beaconId }, { $set: { attachments: [] } }),

  updateBeaconFromApi: async (beaconName, projectId = DEFAULT_PROJECT) => {
    try {
      const beaconsListResponse = await makeRequest(FETCH_BEACONS_LIST, {
        params: {
          projectId: DEFAULT_PROJECT,
        },
      });

      const findedBeacon = beaconsListResponse.filter(el => {
        const regex = new RegExp(`.*(R - ${beaconName})$`);
        const secondRegexp = new RegExp(
          `.*(@[a-z0-9]{1,20}.[a-z]{2,5}[\s]-[\s]${beaconName})`, //eslint-disable-line
          'ig',
        );
        return regex.test(el.description) || secondRegexp.test(el.description);
      })[0];

      if (!findedBeacon) throw errorHandler(SOMETHING_GOES_WRONG);

      const defaultAccount = 'dev9@royaltie.com';

      const defaultProjectResponse = await projectLocalAPI.getOne({
        projectId,
      });

      const currentBeaconName = findedBeacon.beaconName.split('/')[1];
      const defaultAttahcment = await addDefaultAttachment(
        currentBeaconName,
        projectId,
      );

      if (defaultProjectResponse && defaultProjectResponse.hasError)
        throw errorHandler(SOMETHING_GOES_WRONG);

      const newBeaconOptions = {
        ...findedBeacon,
        developerAccount: defaultAccount,
        projectId,
        project: defaultProjectResponse._id,
        orderStatus: ORDER_DEFAULT_STATUS,
        attachments: [defaultAttahcment],
        // add fields here
      };

      const savedBeaconResponse = await beaconsLocalAPI.create(
        newBeaconOptions,
      );

      return beaconsLocalAPI.getWithPopulate({ _id: savedBeaconResponse._id });
    } catch (error) {
      return error;
    }
  },
  getProvisionedBeaconsWithCustomers: async () =>
    beaconModel
      .find({ orderStatus: ORDER_PROVISIONED_STATUS })
      .populate('customer'),
};

export const customersLocalAPI = {
  ...customer,
  customersListDataTable: async (req, options) => {
    const dtParams = prepareDtRequest(req);
    dtParams.search = req.query.keyword
      ? {
          value: req.query.keyword,
          fields: ['firstName', 'lastName', 'email'],
        }
      : {};

    try {
      const dtList = await customer.dataTable(
        Object.assign({}, dtParams, options),
      );
      return dtList;
    } catch (e) {
      console.log(e);
      return e;
    }
  },
  customersAffiliateListDataTable: async (req, options) => {
    const dtParams = prepareDtRequest(req);
    dtParams.find = {
      affiliateCode: {
        $ne: null,
      },
    };
    dtParams.search = req.query.keyword
      ? {
          value: req.query.keyword,
          fields: ['firstName', 'lastName', 'email'],
        }
      : {};

    try {
      const dtList = await customer.dataTable(
        Object.assign({}, dtParams, options),
      );
      return dtList;
    } catch (e) {
      console.log(e);
      return e;
    }
  },
  hasUnassignedBeacons: async (email, beaconsQuantity) => {
    try {
      const currentCustomer = await customersLocalAPI.getOne({ email });
      const customerBeaconsResponse = await beaconsLocalAPI.list({
        customer: currentCustomer._id,
      });

      if (
        (!customerBeaconsResponse && currentCustomer.subscription.quantity) ||
        customerBeaconsResponse.length < beaconsQuantity
      ) {
        return {
          shouldStatusUpdate: true,
          currentUnassignedBeacons:
            beaconsQuantity - customerBeaconsResponse.length,
        };
      }

      return {
        shouldStatusUpdate: false,
        currentUnassignedBeacons: 0,
      };
    } catch (error) {
      return errorHandler(CUSTOM_ERROR, {
        code: 500,
        message: error,
      });
    }
  },
};

export const apiErrorsLocalAPI = {
  ...apiError,
  apiErrorsListDataTable: async (req, options) => {
    const dtParams = prepareDtRequest(req);
    dtParams.search = req.query.keyword
      ? {
          value: req.query.keyword,
          fields: ['title'],
        }
      : {};

    try {
      const dtList = await apiError.dataTable(
        Object.assign({}, dtParams, options),
      );
      return dtList;
    } catch (e) {
      console.log(e);
      return e;
    }
  },
};

export const getAccessLocalAccessToken = async email => {
  try {
    const { accessToken } = await account.getById({ developerAccount: email });
    return accessToken;
  } catch (e) {
    return 'Bad access token';
  }
};

export const getAccessTokenHandler = async projectId => {
  const now = moment().unix();
  const TOKEN_EXPIRE_TIME = 3600;

  const accountAccessTokenInfo = await getAccessLocalAccessToken(projectId);

  if (
    !accountAccessTokenInfo ||
    !accountAccessTokenInfo.createdOn ||
    now - accountAccessTokenInfo.createdOn > TOKEN_EXPIRE_TIME
  ) {
    const accessToken = await fetchAccessToken(projectId);
    return accessToken;
  }

  return accountAccessTokenInfo.token;
};

// export const prepareDatabaseData = async () => {
//   console.log('start preparing...');
//   const result = await beacon.list({});
//   result.forEach(async b => {
//     if (!b.description || b.customer) return;
//     const customerEmail = b.description.split(' ')[0];
//     const currentCustomer = await customer.list({ email: customerEmail });
//
//     if (!currentCustomer || !currentCustomer[0]) return;
//
//     console.log('updating...');
//
//     b.customer = currentCustomer[0]._id;
//     process.nextTick(() => {
//       b.save();
//     });
//   });
//   console.log('done with preparing...');
// };
//

// export const prepareDatabaseBeacons = async () => {
//   console.log('start preparing...');
//   const result = await beacon.list({});
//   result.forEach(async b => {
//     if (!b.projectId) return;
//
//     const currentProject = await project.list({ projectId: b.projectId });
//
//     if (!currentProject || !currentProject[0]) return;
//
//     console.log('updating...');
//
//     b.project = currentProject[0]._id;
//     process.nextTick(() => {
//       b.save();
//     });
//   });
//   console.log('done with preparing...');
// };
//
// export const prepareDatabaseProjects = async () => {
//   const result = await beacon.list({});
//   result.forEach(async (b) => {
//
//     if (!b.developerAccount) return;
//
//     const currentDeveloperAccountEmail = b.developerAccount;
//
//     const currentDeveloperAccount = await accountModel.findOneAndUpdate(
//       { developerAccount: currentDeveloperAccountEmail },
//       { $setOnInsert: { developerAccount: currentDeveloperAccountEmail } },
//       { upsert: true, new: true }
//     );
//
//     if (!b.projectId) return;
//
//     const newProject = {
//       projectId: b.projectId,
//       account: currentDeveloperAccount._id,
//     };
//
//     const projectResult = await projectModel.findOneAndUpdate(
//       { projectId: b.projectId },
//       { $setOnInsert: newProject },
//       { upsert: true, new: true },
//     );
//
//     b.project = projectResult._id;
//
//     b.save();
//
//   });
//   console.log('done with creating...');
// };

beaconUpdate();
