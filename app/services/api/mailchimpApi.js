import config from '../../../config/config.json';
import {
  MAILCHIMP_LIST_ID,
  MAILCHIMP_SUBSCRIBED_STATUS,
} from '../../shared/constants';

import Helpers from '../../shared/utils';

const { sendRequest } = Helpers;

export const addNewListMember = (email, firstName, lastName) => {
  const preparedBaseUrl = config.mailchimp.baseUrl.replace(
    '{subdomain}',
    config.mailchimp.subdomain,
  );
  const url = `${preparedBaseUrl}/lists/:list_id/members`;
  const auth = {
    user: 'any',
    password: config.mailchimp.apiKey,
  };
  const payload = {
    email_address: email,
    status: MAILCHIMP_SUBSCRIBED_STATUS,
    merge_fields: {
      FNAME: firstName,
      LNAME: lastName,
    },
  };

  return sendRequest({
    url,
    params: {
      list_id: MAILCHIMP_LIST_ID,
    },
    method: 'POST',
    auth,
    data: payload,
  });
};
