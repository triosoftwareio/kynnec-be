import Recurly from 'node-recurly';
import config from '../../../config/config.json';

const recurlyApi = new Recurly(config.recurly);

const recurlyPromiseApi = (() =>
  Object.keys(recurlyApi).reduce((promisfied, entity) => {
    const futurePromise = promisfied;
    if (!futurePromise[entity]) futurePromise[entity] = {};
    Object.keys(recurlyApi[entity]).forEach(methodName => {
      futurePromise[entity][methodName] = (...params) =>
        new Promise(resolve => {
          recurlyApi[entity][methodName].apply(recurlyApi[entity], [
            ...params,
            resolve,
          ]);
        });
    });
    return futurePromise;
  }, {}))();

export default {
  ...recurlyPromiseApi,
  createAccount: params => recurlyPromiseApi.accounts.create(params),
  getAccount: params => recurlyPromiseApi.accounts.get(params),
  updateAccount: (code, params) =>
    recurlyPromiseApi.accounts.update(code, params),
  getSubsList: (code, params) =>
    recurlyPromiseApi.subscriptions.listByAccount(code, params),
  createSubscription: params => recurlyPromiseApi.subscriptions.create(params),
  updateSubscription: (uid, params) =>
    recurlyPromiseApi.subscriptions.update(uid, params),
  createCharge: (code, params) =>
    recurlyPromiseApi.adjustments.create(code, params),
  deleteCharge: code => recurlyPromiseApi.adjustments.remove(code),
  cancelSub: params => recurlyPromiseApi.subscriptions.cancel(params),
  terminateSub: code =>
    recurlyPromiseApi.subscriptions.terminate(code, { refund_type: 'full' }),
  reActivateSub: params => recurlyPromiseApi.subscriptions.reactivate(params),
  postponeSub: (code, params) =>
    recurlyPromiseApi.subscriptions.postpone(code, params),
  postInvoice: (code, params) =>
    recurlyPromiseApi.invoices.create(code, params),
};
