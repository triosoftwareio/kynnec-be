import {
  MARKETING_URL,
  MARKETING_API_KEY,
} from '../../shared/constants/marketingConst';

import Helpers from '../../shared/utils';

const { sendRequest } = Helpers;

// Use to add a new user to the marketing database
// Use Add user once the user has successfully register and activated his subscription
// Make sure to pass all the required parameters
// If the user did not register with a sponsor then set sponsor to 0
export const addNewUser = (
  email,
  name,
  subscriptionAmount,
  status,
  timestamp,
  sponsorId,
) => {
  const formData = {
    apiKey: MARKETING_API_KEY,
    email,
    name,
    subscription_amount: subscriptionAmount,
    status,
    timestamp,
    sponsor_id: sponsorId,
  };

  return sendRequest({
    url: `${MARKETING_URL}/user/add`,
    method: 'POST',
    params: formData,
  });
};

// Use update user when a user needs to be changed because he cancelled a subscription, set the status parameter to 'Inactive'
// Use it to also reactivate a user if his subscription is reactivated , set the status parameter to 'Active' for this
// You may also use it to update a users information.
// Form data is a object accepts email which is required, name (optional), sponsor_id (optional), subscription_amount (optional), status (optional), timestamp (optional)
// as keys in a key: val object
export const updateUser = formData =>
  sendRequest({
    url: `${MARKETING_URL}/user/update`,
    method: 'POST',
    params: formData,
  });

// Only use the delete users in extreme cases that you are sure you want to delete the users, there is integrity constraints on child rows
// to prevent orphaned rows.
// Accepts email as a string
export const deleteUser = email => {
  const formData = {
    apiKey: MARKETING_API_KEY,
    email,
  };

  return sendRequest({
    url: `${MARKETING_URL}/user/delete`,
    method: 'POST',
    params: formData,
  });
};

// Use this to generate user downline, pass email as string and the end date being the date you want to calculate until
// This means if a user needs to see commissions up until end of december you have to pass a date like so YYYY-12-31 23:59:59
export const getUserDownline = (email, payoutEndDate) => {
  const formData = {
    apiKey: MARKETING_API_KEY,
    email,
    payoutEndDate,
  };

  return sendRequest({
    url: `${MARKETING_URL}/user/downline/get`,
    method: 'POST',
    params: formData,
  });
};

// Use to generate a user payout report for a specific user
export const generateReport = (
  email,
  payoutEndDate,
  cashBonusStartDate,
  cashBonusEndDate,
) => {
  const formData = {
    apiKey: MARKETING_API_KEY,
    email,
    payoutEndDate,
    cashBonusStartDate,
    cashBonusEndDate,
  };

  return sendRequest({
    url: `${MARKETING_URL}/user/report/get`,
    method: 'POST',
    params: formData,
  });
};

// Generates payout report for all users , all parameters are required.
export const generatePayoutReport = (
  payoutEndDate,
  cashBonusStartDate,
  cashBonusEndDate,
) => {
  const formData = {
    apiKey: MARKETING_API_KEY,
    payoutEndDate,
    cashBonusStartDate,
    cashBonusEndDate,
  };

  return sendRequest({
    url: `${MARKETING_URL}/users/report/payout/get`,
    method: 'POST',
    params: formData,
  });
};

export const addHistoricalSubscription = (email, date, amount) => {
  const formData = {
    apiKey: MARKETING_API_KEY,
    email,
    date,
    amount,
  };

  return sendRequest({
    url: `${MARKETING_URL}/subscription/monthly/add`,
    method: 'POST',
    params: formData,
  });
};

export const calculateSuperAffiliateCommission = (email, endDate) => {
  const formData = {
    apiKey: MARKETING_API_KEY,
    email,
    endDate,
  };

  return sendRequest({
    url: `${MARKETING_URL}/calculate/super-affiliate`,
    method: 'POST',
    params: formData,
  });
};
