import h from 'highland';
import schedule from 'node-schedule';
import { makeRequest } from './googleApi';
import Project from '../models/projectModel';
import Beacon from '../models/beaconsModel';
import {
  INACTIVE_STATUS,
  FETCH_BEACONS_LIST,
  FETCH_ATTACHMENT,
} from '../../shared/constants';

const { projectModel } = Project;
const { beaconModel } = Beacon;

const getBeaconsList = async el => {
  const beaconsList = await makeRequest(FETCH_BEACONS_LIST, {
    params: {
      projectId: el.projectId,
    },
  });

  beaconsList.map(item => {
    const data = item;
    delete data.advertisedId;
    data.projectId = el.projectId;
    data.attachments = [];
    return data;
  });

  return beaconsList;
};

const getBeaconAttachments = async el => {
  const data = el;
  const attachmentList = await makeRequest(FETCH_ATTACHMENT, {
    params: {
      projectId: el.projectId,
      name: el.beaconName.split('/')[1],
    },
  });

  data.attachments = attachmentList;

  return data;
};

/* eslint-disable */
export const beaconUpdate = () => {
  schedule.scheduleJob({ hour: 4, minute: 0 }, async () => {
    const projectList = await projectModel.find({
      status: { $ne: INACTIVE_STATUS },
      isBlockedByGoogle: { $ne: true },
    });

    const data = h(projectList);

    data
      .flatMap(el => h(getBeaconsList(el)))
      .sequence()
      .flatMap(el => h(getBeaconAttachments(el)))
      .flatMap(el =>
        h(
          beaconModel.findOneAndUpdate(
            {
              beaconName: el.beaconName,
            },
            el,
            {
              new: true,
              upsert: true,
            },
          ),
        ),
      )
      .toArray(el => {
        console.log(el.length, 'UPDATED');
      });
  });
};
/* eslint-enable */
