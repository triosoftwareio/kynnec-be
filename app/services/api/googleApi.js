import btoa from 'btoa';
import errorHandler from '../../controllers/errorHandler';
import config from '../../../config/config.json';
import { apiErrorsLocalAPI } from './localApi';

import {
  FETCH_BEACONS_LIST,
  FETCH_BEACON,
  DELETE_BEACON,
  DECOMMISSION_BEACON,
  ACTIVATE_BEACON,
  DEACTIVATE_BEACON,
  REGISTER_BEACON,
  CREATE_ATTACHMENT,
  DELETE_ATTACHMENT,
  FETCH_ATTACHMENT,
  MISSED_PARAMS,
  SOMETHING_GOES_WRONG,
} from '../../shared/constants';

import Helpers from '../../shared/utils';
import { beaconAPI, attachmentAPI } from '../models';

const { sendRequest } = Helpers;

const paramsValidation = (methodName, rest = {}) => {
  const validationRules = {
    [FETCH_BEACONS_LIST]: {
      params: [],
      body: false,
    },
    [FETCH_BEACON]: {
      params: ['projectId'],
      body: false,
    },
    [DELETE_BEACON]: {
      params: ['projectId', 'name'],
      body: false,
    },
    [DECOMMISSION_BEACON]: {
      params: ['projectId', 'name'],
      body: false,
    },
    [ACTIVATE_BEACON]: {
      params: ['projectId', 'name'],
      body: false,
    },
    [DEACTIVATE_BEACON]: {
      params: ['projectId', 'name'],
      body: false,
    },
    [REGISTER_BEACON]: {
      params: ['projectId'],
      body: true,
    },
    [CREATE_ATTACHMENT]: {
      params: ['projectId', 'name'],
      body: true,
    },
    [DELETE_ATTACHMENT]: {
      params: ['attachmentName'],
      body: false,
    },
    [FETCH_ATTACHMENT]: {
      params: ['name', 'projectId'],
      body: false,
    },
  };

  const currentMethodRules = validationRules[methodName];
  const { params, body } = rest;
  const missedParams = currentMethodRules.params.filter(
    param => !params[param],
  );
  const missedBody = currentMethodRules.body
    ? !(body || Object.keys(body).length)
    : false;

  return { missedParams, missedBody };
};

const requestRetryHandler = (
  { projectId, errorCode, errorMessage, triesCounter, tryTime } = {},
) => {
  const apiErrorEntity = {
    projectId,
    errorCode,
    errorMessage,
    triesCounter,
    tryTime,
  };
  return apiErrorsLocalAPI.create(apiErrorEntity);
};

const apiRequests = {
  [FETCH_BEACONS_LIST]: ({ ...rest } = {}) => beaconAPI.list({ ...rest }),
  [FETCH_BEACON]: ({ ...rest } = {}) => beaconAPI.fetch({ ...rest }),
  [DELETE_BEACON]: ({ ...rest } = {}) => beaconAPI.remove({ ...rest }),
  [REGISTER_BEACON]: ({ ...rest } = {}) => beaconAPI.register({ ...rest }),
  [DECOMMISSION_BEACON]: ({ ...rest } = {}) =>
    beaconAPI.decommission({ ...rest }),
  [ACTIVATE_BEACON]: ({ ...rest } = {}) => beaconAPI.activate({ ...rest }),
  [DEACTIVATE_BEACON]: ({ ...rest } = {}) => beaconAPI.deactivate({ ...rest }),
  [CREATE_ATTACHMENT]: ({ ...rest } = {}) => attachmentAPI.create({ ...rest }),
  [DELETE_ATTACHMENT]: ({ ...rest } = {}) => attachmentAPI.delete({ ...rest }),
  [FETCH_ATTACHMENT]: ({ ...rest } = {}) => attachmentAPI.list({ ...rest }),
};

export const makeRequest = (methodName, options = {}) => {
  const validation = paramsValidation(methodName, options);
  if (validation.missedParams.length || validation.missedBody)
    return errorHandler(MISSED_PARAMS, validation);

  return apiRequests[methodName]({
    retryCallback: requestRetryHandler,
    ...options,
  });
};

export const fetchAccessToken = projectId => {
  const FETCH_TOKEN_URL = `${config.inigoEndpoint}/beacon/token/:projectId`;
  const token = config.accessToken;
  const headers = {
    clientId: token,
  };

  return sendRequest({
    url: FETCH_TOKEN_URL,
    headers,
    params: { projectId },
  });
};

const createBeaconWithAttachment = async (projectId, beacon) => {
  const newBeacon = await makeRequest(REGISTER_BEACON, {
    params: { projectId },
    body: beacon,
  });

  if (beacon.attachments && beacon.attachments.length) {
    const attachmentData = {
      namespacedType: 'com.google.nearby/en',
      data: btoa(JSON.stringify(beacon.attachments[0])),
    };
    await makeRequest(CREATE_ATTACHMENT, {
      params: { projectId, name: newBeacon.beaconName },
      body: attachmentData,
    });
  }
  return beacon._id;
};

const migrateBeacon = async ({ projectFrom, beacon, projectTo }) => {
  try {
    await makeRequest(DELETE_BEACON, {
      params: { projectId: projectFrom, name: beacon.beaconName },
    });

    const createBeaconResult = await createBeaconWithAttachment(
      projectTo,
      beacon,
    );
    if (createBeaconResult.hasError) throw Error;

    return createBeaconResult;
  } catch (error) {
    createBeaconWithAttachment(projectFrom, beacon);
    return false;
  }
};

export const migrateBeacons = (projectFrom, beacons, projectTo) =>
  beacons.map(beacon => migrateBeacon({ projectFrom, beacon, projectTo }));

export const updateAttachment = async (
  projectId,
  beaconName,
  attachmentName,
  newAttachment,
) => {
  try {
    const deleteAttachmentParams = {
      projectId,
      name: beaconName,
      attachmentName,
    };

    const deleteAttachmentResponse = await makeRequest(DELETE_ATTACHMENT, {
      params: deleteAttachmentParams,
    });

    console.log(deleteAttachmentResponse);

    // if (deleteAttachmentResponse.hasError) throw deleteAttachmentResponse;

    const createAttachmentOptions = {
      params: {
        projectId,
        name: beaconName,
      },
      body: newAttachment,
    };

    const createAttachmentResponse = await makeRequest(
      CREATE_ATTACHMENT,
      createAttachmentOptions,
    );

    if (createAttachmentResponse.hasError) throw createAttachmentResponse;

    return createAttachmentResponse;
  } catch (error) {
    return error;
  }
};

export const addDefaultAttachment = async (beaconName, projectId) => {
  try {
    const defaultMessage = {
      title: 'Royaltie',
      url: 'https://www.royaltie.com',
      targeting: [],
      namespacedType: 'com.google.nearby/en',
    };

    const createAttachmentOptions = {
      params: {
        projectId,
        name: beaconName,
      },
      body: defaultMessage,
    };

    const createAttachmentResponse = await makeRequest(
      CREATE_ATTACHMENT,
      createAttachmentOptions,
    );

    if (createAttachmentResponse.hasError)
      throw errorHandler(SOMETHING_GOES_WRONG);

    return createAttachmentResponse;
  } catch (error) {
    return error;
  }
};
